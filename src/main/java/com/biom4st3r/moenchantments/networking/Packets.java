package com.biom4st3r.moenchantments.networking;

import net.minecraft.network.PacketByteBuf;

import io.netty.buffer.Unpooled;

public final class Packets {
    public static PacketByteBuf newPbb() {
        return new PacketByteBuf(Unpooled.buffer());
    }
}