package com.biom4st3r.moenchantments.logic;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Queue;
import java.util.function.Supplier;

import com.biom4st3r.moenchantments.MoEnchantsConfig;
import com.biom4st3r.moenchantments.ModInit;
import com.biom4st3r.moenchantments.registration.EnchantmentRegistry;
import com.biom4st3r.moenchantments.util.TagHelper;
import com.google.common.collect.Lists;
import com.google.common.collect.Queues;

import org.jetbrains.annotations.Nullable;

import it.unimi.dsi.fastutil.longs.LongOpenHashSet;
import net.minecraft.block.Block;
import net.minecraft.block.BlockState;
import net.minecraft.block.entity.BlockEntity;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.item.ItemStack;
import net.minecraft.registry.tag.BlockTags;
import net.minecraft.registry.tag.TagKey;
import net.minecraft.sound.SoundCategory;
import net.minecraft.sound.SoundEvents;
import net.minecraft.stat.Stats;
import net.minecraft.text.Text;
import net.minecraft.util.Formatting;
import net.minecraft.util.Identifier;
import net.minecraft.util.Util;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.World;

public final class VeinMinerLogic {

    public static void events() {
    }

    public static final Supplier<TagKey<Block>> ORES = TagHelper.memoizedSupplier(() -> TagHelper.block(new Identifier("moenchantments:ores")));

    public static void tryVeinMining(World world, BlockPos blockPos, BlockState blockState, PlayerEntity pe) {
        if (world.isClient) return; // Early return: Server only.
        if (pe == null) return; // Early return: Patch for Adorn CarpetedBlock.
        if (pe.isCreative()) return; // Early return: Creative mode
        if (!pe.canHarvest(blockState)) return; // Early return: Slippery support.
        
        ItemStack tool = pe.getMainHandStack();
        int veinMinerLvl = EnchantmentRegistry.VEINMINER.getLevel(tool);
        int treeFellerLvl = EnchantmentRegistry.TREEFELLER.getLevel(tool);

        if (tool == ItemStack.EMPTY) return; // Early return: No held item.
        if (veinMinerLvl + treeFellerLvl <= 0) return; // Early return: if it doesn't have the enchantments.

        Block currentType = blockState.getBlock();
        int brokenBlocks = 0;

        if (treeFellerLvl > 0 && isValidBlockForAxe(blockState)) {
            brokenBlocks = doVeinMiner(blockState, world, blockPos, MoEnchantsConfig.config.TreeFellerMaxBreakByLvl[treeFellerLvl-1], pe, tool);
            ModInit.logger.debug("%s block broken", brokenBlocks);
        } else if (veinMinerLvl > 0 && isValidBlockForPick(blockState)) {
            brokenBlocks = doVeinMiner(blockState, world, blockPos, MoEnchantsConfig.config.VeinMinerMaxBreakByLvl[veinMinerLvl-1], pe, tool);
            ModInit.logger.debug("%s block broken", brokenBlocks);
        }

        pe.increaseStat(Stats.MINED.getOrCreateStat(currentType), brokenBlocks);
    }

    private static boolean isValidBlockForPick(BlockState state) {
        
        return state.isIn(ORES.get());
    }

    private static boolean isValidBlockForAxe(BlockState state) {
        return state.isIn(BlockTags.LOGS);
        // return BlockTags.LOGS.contains(state.getBlock());
    }

    public static ArrayList<BlockPos> getSameBlocks(World w, BlockPos source, Block type) {
        ArrayList<BlockPos> t = Lists.newArrayList();
        BlockPos[] poses = new BlockPos[] {
            source.down().north(), source.down().east(), source.down().west(), source.down().south(),
            source.up(), source.down(), source.north(), source.east(), source.south(), source.west(),
            source.up().south(), source.up().east(), source.up().west(), source.up().north()};
        
        for (BlockPos pos : poses) {
            if (w.getBlockState(pos).getBlock() == type) {
                t.add(pos);
            }
        }
        Collections.shuffle(t);
        return t;
    }

    private static int doVeinMiner(BlockState blockState, World world, BlockPos blockPos, int maxBlocks, PlayerEntity pe, ItemStack tool) {
        ModInit.logger.debug("BlockType %s", blockState.getBlock());
        // int blocksBroken = 0;
        LongOpenHashSet used = new LongOpenHashSet(29);
        Queue<BlockPos> toBreak = Util.make(Queues.newArrayBlockingQueue(29), queue -> queue.add(blockPos));

        while(!toBreak.isEmpty() && used.size() <= maxBlocks) {
            BlockPos currPos = toBreak.poll();
            BlockState currentState = world.getBlockState(currPos);
            used.add(currPos.asLong());
            if (toBreak.size() < 50) {
                getSameBlocks(world, currPos, blockState.getBlock())
                    .stream()
                    .filter(pos -> !used.contains(pos.asLong()))
                    .forEach(toBreak::offer);
                    ;
            }
            
            breakBlock(currentState, world, currPos, null, pe, tool, blockPos);

            if (MoEnchantsConfig.config.ProtectItemFromBreaking && tool.isDamageable() && tool.getMaxDamage()-tool.getDamage() < 5) {
                pe.world.playSound(null, currPos, SoundEvents.ENTITY_ITEM_BREAK, SoundCategory.PLAYERS, 1f, 1f);
                pe.sendMessage(Text.translatable("dialog.moenchantments.tool_damage_warning_veinminer").formatted(Formatting.GREEN), false);
                break;
            }
        }
        ModInit.lockAutoSmeltSound = false;
        pe.addExhaustion(0.005F * used.size());
        return used.size();
    }

    private static void breakBlock(BlockState state, World world, BlockPos pos, @Nullable BlockEntity entity, PlayerEntity pe, ItemStack tool, BlockPos itemDropPos) {
        // May not be compatible with protection mods
        Block.dropStacks(state, world, itemDropPos, null, pe, tool);
        world.breakBlock(pos, false);
        state.getBlock().onBreak(world, pos, state, pe);
        tool.postMine(world, state, pos, pe);
        
        tool.damage(1, pe,(playerEntity_1)  ->  {
            playerEntity_1.sendToolBreakStatus(pe.getActiveHand());
        });
    }
}