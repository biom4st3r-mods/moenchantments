package com.biom4st3r.moenchantments.logic;

import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import com.biom4st3r.moenchantments.MoEnchantsConfig;
import com.biom4st3r.moenchantments.entities.LivingItemEntity;
import com.biom4st3r.moenchantments.registration.EnchantmentRegistry;
import com.biom4st3r.moenchantments.util.TagHelper;

import net.minecraft.entity.mob.PatrolEntity;
import net.minecraft.entity.passive.VillagerEntity;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.registry.Registries;
import net.minecraft.server.world.ServerWorld;
import net.minecraft.util.Identifier;
import net.minecraft.world.ServerWorldAccess;

public class LivingItemLogic {
    public static final List<Item> PILLAGER_TOOLS = Stream.of(MoEnchantsConfig.config.living_item_pillager_patrol_tools)
    .map(s -> {
        if (s.startsWith("#")) return TagHelper.toStream(Registries.ITEM, TagHelper.item(s.replace("#","")));
        else return Stream.of(Registries.ITEM.get(new Identifier(s)));
    }).flatMap(s->s)
    .collect(Collectors.toList());
    public static final List<Item> VILLAGER_TOOLS = Stream.of(MoEnchantsConfig.config.living_item_villager_tools)
    .map(s -> {
        if (s.startsWith("#")) return TagHelper.toStream(Registries.ITEM, TagHelper.item(s.replace("#","")));
        else return Stream.of(Registries.ITEM.get(new Identifier(s)));
    }).flatMap(s->s)
    .collect(Collectors.toList());;

    public static void spawnWithPillager(ServerWorld world, PatrolEntity patrolEntity) {
        if (!EnchantmentRegistry.LIFE_LIKE.isEnabled()) return;
        Item item = PILLAGER_TOOLS.get(world.getRandom().nextInt(PILLAGER_TOOLS.size()));
        ItemStack is = new ItemStack(item);
        is.addEnchantment(EnchantmentRegistry.LIFE_LIKE.asEnchantment(), 1);
        LivingItemEntity lie = new LivingItemEntity(world, is, patrolEntity);
        lie.updatePositionAndAngles(patrolEntity.getX(), patrolEntity.getY(), patrolEntity.getZ(), patrolEntity.getYaw(), patrolEntity.getPitch());
        world.spawnEntity(lie);
    }

    public static void spawnWithVillager(ServerWorldAccess world, VillagerEntity ve) {
        if (!EnchantmentRegistry.LIFE_LIKE.isEnabled()) return;
        Item item = VILLAGER_TOOLS.get(world.getRandom().nextInt(VILLAGER_TOOLS.size()));
        ItemStack stack = new ItemStack(item);
        stack.addEnchantment(EnchantmentRegistry.LIFE_LIKE.asEnchantment(), 1);
        LivingItemEntity entity = new LivingItemEntity(ve.getWorld(), stack, ve);
        entity.refreshPositionAndAngles(ve.getBlockPos(), ve.getYaw(), ve.getPitch());
        world.spawnEntity(entity);
    }
}
