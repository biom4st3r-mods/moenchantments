package com.biom4st3r.moenchantments.logic;

import static com.biom4st3r.moenchantments.registration.EnchantmentRegistry.ARROW_CHAOS;
import static com.biom4st3r.moenchantments.registration.EnchantmentRegistry.AUTOSMELT;
import static com.biom4st3r.moenchantments.registration.EnchantmentRegistry.ENDERPROTECTION;
import static com.biom4st3r.moenchantments.registration.EnchantmentRegistry.FILTER_FEEDER;
import static com.biom4st3r.moenchantments.registration.EnchantmentRegistry.GRAPNEL;
import static com.biom4st3r.moenchantments.registration.EnchantmentRegistry.POTIONRETENTION;
import static com.biom4st3r.moenchantments.registration.EnchantmentRegistry.SLIPPERY;
import static com.biom4st3r.moenchantments.registration.EnchantmentRegistry.TAMEDPROTECTION;
import static com.biom4st3r.moenchantments.registration.EnchantmentRegistry.TREEFELLER;
import static com.biom4st3r.moenchantments.registration.EnchantmentRegistry.VEINMINER;
import static com.biom4st3r.moenchantments.registration.EnchantmentRegistry.VOID_STEP;
import static com.biom4st3r.moenchantments.registration.EnchantmentRegistry.enchantment;

import java.util.function.Predicate;

import com.biom4st3r.moenchantments.util.DoMagicThing;

import biom4st3r.libs.moenchant_lib.ExtendedEnchantment;
import net.fabricmc.fabric.api.event.player.UseItemCallback;
import net.minecraft.block.BlockState;
import net.minecraft.block.Blocks;
import net.minecraft.enchantment.Enchantment;
import net.minecraft.enchantment.EnchantmentHelper;
import net.minecraft.inventory.Inventory;
import net.minecraft.item.ItemStack;
import net.minecraft.server.network.ServerPlayerEntity;
import net.minecraft.util.TypedActionResult;
import net.minecraft.util.hit.BlockHitResult;
import net.minecraft.util.hit.HitResult.Type;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.Box;
import net.minecraft.world.World;

public class EventCollection {

    public static void ifEnchantment(ExtendedEnchantment enchantment, Runnable r) {
        if (enchantment.isEnabled()) r.run();
    }

    public static void ifOrEnchantment(Runnable r, ExtendedEnchantment... enchantments) {
        for (ExtendedEnchantment enchant : enchantments) {
            if (enchant.isEnabled()) {
                r.run();
                break;
            }
        }
    }

    public static void init() {
        ifEnchantment(AUTOSMELT, AlphafireLogic::events);
        ifEnchantment(ARROW_CHAOS, ChaosArrowLogic::events);
        ifEnchantment(ENDERPROTECTION, EnderProtectionLogic::events);
        ifEnchantment(VOID_STEP, EndStep::events);
        ifEnchantment(TAMEDPROTECTION, Familiarity::events);
        ifEnchantment(FILTER_FEEDER, FilterFeeder::events);
        ifEnchantment(GRAPNEL, GrapnelLogic::events);
        ifEnchantment(POTIONRETENTION, PotionRetention::events);
        ifEnchantment(SLIPPERY, Slippery::events);
        ifOrEnchantment(VeinMinerLogic::events, VEINMINER, TREEFELLER);

        ifEnchantment(enchantment, DoMagicThing::init);
        if (!enchantment.isEnabled()) return;
        UseItemCallback.EVENT.register((player, world, hand) -> {
            ItemStack stack = player.getStackInHand(hand);
            if (!world.isClient && stack.isOf(DoMagicThing.getTime()) && DoMagicThing.knowsWhatTimeItIs(stack) && player.raycast(30, 1.0F, false) instanceof BlockHitResult result && result.getType() != Type.MISS) {
                BlockState state = world.getBlockState(result.getBlockPos());
                if (state.isOf(Blocks.BARREL) && DoMagicThing.checkTime(world, result.getBlockPos())) {
                    if (world.getBlockEntity(result.getBlockPos()) instanceof Inventory barrel) {
                        boolean spawned = false;
                        for (int i = 0; i < barrel.size(); i++) {
                            ItemStack cursed = barrel.getStack(i);
                            if (isCursed(cursed)) {
                                if (!spawned) {
                                    spawned = true;
                                    DoMagicThing.executeAtSeven30(world, result.getBlockPos(), (ServerPlayerEntity)player);
                                }
                                cursed.removeSubNbt("Enchantments");
                                EnchantmentHelper.enchant(player.getRandom(), cursed, 30, true);
                                if (player.getRandom().nextDouble() < 0.00004) {
                                    cursed.getOrCreateNbt().putBoolean("Unbreakable", true);
                                }
                            }
                        }
                    }
                    stack.damage(420, player, p -> p.sendToolBreakStatus(hand));
                }
            }
            return TypedActionResult.pass(null);
        });
    }

    private static boolean isCursed(ItemStack stack) {
        if (stack.isEmpty()) return false;
        for (Enchantment enchant : EnchantmentHelper.fromNbt(stack.getEnchantments()).keySet()) {
            if (enchant.isCursed()) return true;
        }
        return false;
    }

    public static boolean checkBox(World world, Box box, Predicate<BlockState> condition) {
        boolean result = true;
        for (int x = (int) box.minX; x < box.maxX; x++) {
            for (int y = (int) box.minY; y < box.maxY; y++) {
                for (int z = (int) box.minZ; z < box.maxZ; z++) {
                    result &= condition.test(world.getBlockState(new BlockPos(x, y, z)));
                }
            }
        }
        return result;
    }
}
