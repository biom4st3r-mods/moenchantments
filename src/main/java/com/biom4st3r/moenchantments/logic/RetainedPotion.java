package com.biom4st3r.moenchantments.logic;

import java.io.Closeable;
import java.io.IOException;
import java.util.ArrayDeque;
import java.util.Queue;
import java.util.concurrent.locks.ReentrantLock;
import java.util.function.Consumer;

import com.biom4st3r.moenchantments.MoEnchantsConfig;
import com.biom4st3r.moenchantments.registration.EnchantmentRegistry;

import net.minecraft.entity.LivingEntity;
import net.minecraft.entity.effect.StatusEffect;
import net.minecraft.entity.effect.StatusEffectInstance;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.NbtCompound;
import net.minecraft.registry.Registries;
import net.minecraft.util.Identifier;

/**
 * Object Pool
 */
public class RetainedPotion implements Closeable {

    private RetainedPotion() {}

    private static final String KEY = "biom4st3rpotionretainer";
    private static final ReentrantLock LOCK = new ReentrantLock();
    private static final Queue<RetainedPotion> POOL = new ArrayDeque<>();

    public static int debug_getRtPoolSize() {
        return POOL.size();
    }

    private static void lockPool() {
        LOCK.lock();
    }

    private static void releasePool() {
        LOCK.unlock();
    }
    // Object reuse
    private static RetainedPotion getObject(ItemStack stack) {
        RetainedPotion rt;
        lockPool();
        if (!POOL.isEmpty()) {
            rt = POOL.poll();
            releasePool();
        } else {
            releasePool();
            rt = new RetainedPotion();
        }

        rt.stack = stack;

        return rt;
    }

    public boolean isEmpty() {
        return this.effect == null;
    }

    StatusEffect effect = null;
    int amp = -1;
    int charges = -1;
    ItemStack stack = ItemStack.EMPTY;
    // int fake_lvl = -1;

    public void setPotionEffectAndCharges(StatusEffectInstance effect) {
        int tokens = (int) Math.ceil( (effect.getDuration() / 20.0) / 10.0 );
        if (this.effect == null) {
            this.effect = effect.getEffectType();
            this.amp = effect.getAmplifier();
            this.charges = Math.min(tokens, this.getMaxCharges());
        } else if (this.effect == effect.getEffectType()) {
            this.addCharges(tokens);
        }
    }

    public int getMaxCharges() {
        // if (this.fake_lvl != -1) return fake_lvl; 
        return EnchantmentRegistry.POTIONRETENTION.getLevel(this.stack) * MoEnchantsConfig.config.perLevelChargeMultiplierForPotionRetention; 
    }

    public void toTag(NbtCompound tag) {
        NbtCompound ct = new NbtCompound();
        if (effect != null) {
            ct.putString("effect", Registries.STATUS_EFFECT.getId(effect).toString());
            ct.putInt("amp", amp);
            ct.putInt("charges", charges);
        }
        tag.put(KEY, ct);
    }

    public StatusEffect getEffect() {
        return effect;
    }
    public int getCharges() {
        return charges;
    }
    public int getAmp() {
        return this.amp;
    }

    public void addCharges(int val) {
        this.charges = Math.min(this.getMaxCharges(), val + charges);
        if (this.charges <= 0) {
            this.erase();
        }
    }

    public void useCharge(LivingEntity entity) {
        if (this.effect != null) {
            StatusEffectInstance instance = new StatusEffectInstance(effect, 5*20, this.amp);
            this.addCharges(-1);
            PotionRetention.applyRetainedPotionEffect(entity, instance);
        }
    }

    public static boolean borrowFirstRetainedPotion(Consumer<RetainedPotion> consumer, ItemStack... stacks) {
        for (ItemStack stack : stacks) {
            if (EnchantmentRegistry.POTIONRETENTION.hasEnchantment(stack)) {
                RetainedPotion rt = RetainedPotion.fromStack(stack);
                consumer.accept(rt);
                rt.applyAndRelease();
                return true;
            }
        }
        return false;
    }

    public static boolean borrowRetainedPotion(ItemStack stack, Consumer<RetainedPotion> consumer) {
        if (!EnchantmentRegistry.POTIONRETENTION.hasEnchantment(stack)) {
            return false;
        }
        RetainedPotion rt = RetainedPotion.fromStack(stack);
        consumer.accept(rt);
        rt.applyAndRelease();
        return true;
    }

    // public static boolean borrowRetainedPotion_ignoreEnchantment(ItemStack stack, Consumer<RetainedPotion> consumer, int enchantment_level) {
    //     RetainedPotion rt = RetainedPotion.fromStack(stack);
    //     if (!EnchantmentRegistry.POTIONRETENTION.hasEnchantment(stack)) {
    //         rt.fake_lvl = enchantment_level;
    //     }
    //     consumer.accept(rt);
    //     rt.applyAndRelease();
    //     return true;
    // }
    private static RetainedPotion fromStack(ItemStack stack) {
        RetainedPotion rt = getObject(stack);
        if (stack.getSubNbt(KEY) != null) {
            NbtCompound ct = stack.getNbt().getCompound(KEY);
            rt.effect = Registries.STATUS_EFFECT.get(new Identifier(ct.getString("effect")));
            rt.amp = ct.getInt("amp");
            rt.charges = ct.getInt("charges");
        }
        return rt;
    }

    public void erase() {
        this.effect = null;
        this.amp = -1;
        this.charges = -1;
        // this.fake_lvl = -1;
    }

    public void applyAndRelease() {
        this.toTag(stack.getOrCreateNbt());
        this.erase();
        this.stack = ItemStack.EMPTY;
        lockPool();
        POOL.add(this);
        releasePool();
    }

    @Override
    public void close() throws IOException {
        this.applyAndRelease();
    }
}
