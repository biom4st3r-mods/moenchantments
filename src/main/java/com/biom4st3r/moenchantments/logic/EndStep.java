package com.biom4st3r.moenchantments.logic;

import java.util.List;
import java.util.Optional;

import net.minecraft.item.ItemStack;
import net.minecraft.item.Items;
import net.minecraft.network.PacketByteBuf;
import net.minecraft.particle.ParticleTypes;
import net.minecraft.server.network.ServerPlayerEntity;
import net.minecraft.util.Identifier;
import net.minecraft.util.TypedActionResult;
import net.minecraft.util.hit.BlockHitResult;
import net.minecraft.util.hit.HitResult;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.Vec3d;
import net.minecraft.world.World;

import net.fabricmc.api.EnvType;
import net.fabricmc.fabric.api.event.player.UseItemCallback;
import net.fabricmc.fabric.api.networking.v1.ServerPlayNetworking;
import net.fabricmc.loader.api.FabricLoader;

import biom4st3r.libs.particle_emitter.ParticleEmitter;
import com.biom4st3r.moenchantments.ModInit;
import com.biom4st3r.moenchantments.interfaces.EndStepper;
import com.biom4st3r.moenchantments.logic.EndStepProgressCounter.Stage;
import com.biom4st3r.moenchantments.registration.EnchantmentRegistry;
import com.google.common.collect.Lists;
import com.mojang.datafixers.util.Either;
import io.netty.buffer.Unpooled;


public class EndStep {
    static final TypedActionResult<ItemStack> PASS = TypedActionResult.pass(null);
    
    static final Identifier ENDSTEP_UPDATE_STAGE = new Identifier(ModInit.MODID, "endsteppacket");
    static final Identifier ENDSTEP_INIT = new Identifier(ModInit.MODID, "endstepinit");
    
    public static void updateEndStepState(ServerPlayerEntity pe, Stage stage) {
        PacketByteBuf buf = new PacketByteBuf(Unpooled.buffer());
        buf.writeEnumConstant(stage);
        ServerPlayNetworking.send(pe, ENDSTEP_UPDATE_STAGE, buf);
    }

    public static void initEndStep(ServerPlayerEntity pe, PacketByteBuf buf) {
        ServerPlayNetworking.send(pe, ENDSTEP_INIT, buf);
    }
    
    public static void events() {
        if (FabricLoader.getInstance().getEnvironmentType() != EnvType.SERVER) {
            EndStepClient.client_event();
        }
        
        UseItemCallback.EVENT.register((player, world, hand) -> {
            if (EnchantmentRegistry.VOID_STEP.getLevel(player) == 0) return PASS;
            if (!player.isOnGround()) return PASS;
            ItemStack stack = player.getStackInHand(hand);
            if (!(stack.getItem() == Items.ENDER_PEARL && !player.getItemCooldownManager().isCoolingDown(Items.ENDER_PEARL))) return PASS;
            
            HitResult result = player.raycast(300, 1.0F, false);
            if (result.getType() == HitResult.Type.BLOCK) {
                Either<Boolean, BlockPos> either = isValidPosition(world, ((BlockHitResult) result).getBlockPos());
                BlockPos pos;
                if (either.left().isPresent()) {
                    if (!either.left().get()) return PASS;
                    pos = ((BlockHitResult)result).getBlockPos();
                } else {
                    pos = either.right().get();
                }
                player.getItemCooldownManager().set(Items.ENDER_PEARL, Short.MAX_VALUE);
                if (!player.world.isClient) {
                    ((EndStepper)player).moenchantment$setStepProgress(Optional.of(new EndStepProgressCounter(player, new Vec3d(pos.getX()+0.5D, pos.getY()+1, pos.getZ()+0.5D))));
                    player.sendToolBreakStatus(hand);
                    stack.decrement(1);
                }
                return TypedActionResult.success(stack);
            }
            return PASS;
        });

    }

    /**
     * return true if the position is valid, false if the position is invalid, or a nearby position that is valid 
     * @param world
     * @param pos
     * @return
     */
    public static Either<Boolean, BlockPos> isValidPosition(World world, BlockPos pos) {
        if (!(world.isAir(pos.up()) && world.isAir(pos.up().up()))) return Either.left(false);
        if (world.getBlockState(pos).getMaterial().isSolid()) return Either.left(true);
        if (world.getBlockState(pos.down()).getMaterial().isSolid()) return Either.right(pos.down());
        if (world.getBlockState(pos.down().down()).getMaterial().isSolid()) return Either.right(pos.down().down());
        return Either.left(false);
    }

    public static List<Optional<ParticleEmitter>> createEmitters(World world, Vec3d pos) {
        List<Optional<ParticleEmitter>> list = Lists.newArrayList();
        for (int i = 0; i < 360; i += 45) {
            double current = Math.toRadians(i);
            double x = Math.sin(current);
            double z = Math.cos(current);
            Optional<ParticleEmitter> o = ParticleEmitter.of(ParticleTypes.SOUL_FIRE_FLAME, pos.add(x, 0, z), "randomdouble * 0.03", "randomdouble * 0.15", "randomdouble * 0.03", world, 120);
            o.ifPresent(ParticleEmitter::begin);
            list.add(o);
        }
        return list;
    }
}
