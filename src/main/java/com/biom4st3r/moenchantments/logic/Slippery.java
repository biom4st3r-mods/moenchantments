package com.biom4st3r.moenchantments.logic;

import net.minecraft.entity.EquipmentSlot;
import net.minecraft.entity.ItemEntity;
import net.minecraft.entity.LivingEntity;
import net.minecraft.item.ItemStack;
import net.minecraft.util.TypedActionResult;
import net.minecraft.util.math.MathHelper;
import net.minecraft.util.math.Vec3d;

import net.minecraft.util.math.random.Random;
import net.fabricmc.fabric.api.event.player.UseItemCallback;

import com.biom4st3r.moenchantments.MoEnchantsConfig;
import com.biom4st3r.moenchantments.registration.EnchantmentRegistry;

public class Slippery {
    public static void events() {
        UseItemCallback.EVENT.register((player, world, hand) -> {
            if (EnchantmentRegistry.SLIPPERY.hasEnchantment(player.getStackInHand(hand))) {
                if (Slippery.shouldDropOnItemUse(player.getRandom())) {
                    Slippery.doDrop(player);
                }
            }
            return TypedActionResult.pass(null);
        });
    }

    public static void doDrop(LivingEntity entity) {
        ItemEntity item = new ItemEntity(entity.world, entity.getX(), entity.getY()+(entity.getHeight() * 0.7), entity.getZ(), entity.getMainHandStack());
        item.setVelocity(directionToVelocity(entity.getPitch() - 10, entity.getYaw() + (entity.getRandom().nextInt(90)-45)).multiply(3 * entity.getRandom().nextFloat()));
        entity.equipStack(EquipmentSlot.MAINHAND, ItemStack.EMPTY);
        item.setToDefaultPickupDelay();
        entity.world.spawnEntity(item);
    }

    public static boolean shouldDropOnAttack(Random rand) {
        return MoEnchantsConfig.config.slippery_drop_chance_attack > rand.nextDouble();
    }

    public static boolean shouldDropOnSwap(Random rand) {
        return MoEnchantsConfig.config.slippery_drop_chance_on_swap > rand.nextDouble();
    }

    public static boolean shouldDropOnBlockWithShield(Random rand) {
        return MoEnchantsConfig.config.slippery_drop_shield_when_hit > rand.nextDouble();
    }

    public static boolean shouldDropOnItemUse(Random rand) {
        return MoEnchantsConfig.config.slippery_drop_on_item_use > rand.nextDouble();
    }

    public static Vec3d directionToVelocity(float pitch, float yaw) {
        float toRadian = (float) (Math.PI / 180D);
        float g = MathHelper.sin(pitch * toRadian);
        float j = MathHelper.cos(pitch * toRadian);
        float k = MathHelper.sin(yaw * toRadian);
        float l = MathHelper.cos(yaw * toRadian);
        return new Vec3d(-k * j * 0.3F, -g * 0.3F + 0.1F, l * j * 0.3F);
    }

    public static boolean shouldDropOnMine(Random rand) {
        return MoEnchantsConfig.config.slippery_dropChance_on_mine > rand.nextDouble();
    }
}
