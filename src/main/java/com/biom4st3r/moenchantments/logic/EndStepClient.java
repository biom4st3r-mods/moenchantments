package com.biom4st3r.moenchantments.logic;

import java.util.Optional;

import net.fabricmc.api.EnvType;
import net.fabricmc.api.Environment;
import net.fabricmc.fabric.api.client.networking.v1.ClientPlayNetworking;

import com.biom4st3r.moenchantments.interfaces.EndStepper;
import com.biom4st3r.moenchantments.logic.EndStepProgressCounter.Stage;

@Environment(EnvType.CLIENT)
public class EndStepClient {
    
    static void client_event() {
        ClientPlayNetworking.registerGlobalReceiver(EndStep.ENDSTEP_INIT, (client, handler, buf, sender) -> {
            ((EndStepper)client.player).moenchantment$setStepProgress(Optional.of(new EndStepProgressCounter(client.player, buf)));
        });
        ClientPlayNetworking.registerGlobalReceiver(EndStep.ENDSTEP_UPDATE_STAGE, (client, handler, buf, sender) -> {
            ((EndStepper)client.player).moenchantment$getStepProgress().get().update(buf.readEnumConstant(Stage.class));
        });
    }
}
