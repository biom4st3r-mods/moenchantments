package com.biom4st3r.moenchantments.logic;

import com.biom4st3r.moenchantments.registration.EnchantmentRegistry;

import net.minecraft.entity.LivingEntity;
import net.minecraft.entity.effect.StatusEffectInstance;
import net.minecraft.entity.projectile.ArrowEntity;
import net.minecraft.entity.projectile.ProjectileEntity;
import net.minecraft.entity.projectile.SpectralArrowEntity;
import net.minecraft.potion.Potion;
import net.minecraft.registry.Registries;
import net.minecraft.util.math.random.Random;

/**
 * ChaosArrowLogic
 */
public class ChaosArrowLogic {
    public static void events() {
        
    }
    public static boolean makePotionArrow(LivingEntity attacker, ProjectileEntity projectileEntity, Random random) {
        if (projectileEntity instanceof SpectralArrowEntity) return false;
        
        if (EnchantmentRegistry.ARROW_CHAOS.hasEnchantment(attacker.getActiveItem()) && random.nextInt(3) == 0) {
            Potion potion = Registries.POTION.getRandom(attacker.getRandom()).get().value();
            ArrowEntity arrow = (ArrowEntity) projectileEntity;
            for (StatusEffectInstance sei : potion.getEffects())
                arrow.addEffect(new StatusEffectInstance(sei.getEffectType(),(int)Math.ceil(sei.getDuration()*0.20), sei.getAmplifier()));//Standard is .24
            return true;
        }
        return false;
    }
}