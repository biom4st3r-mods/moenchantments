package com.biom4st3r.moenchantments;

import com.biom4st3r.moenchantments.entities.LivingItemEntity;
import com.biom4st3r.moenchantments.logic.EventCollection;
import com.biom4st3r.moenchantments.registration.Commands;
import com.biom4st3r.moenchantments.registration.EnchantmentRegistry;

import biom4st3r.libs.biow0rks.BioLogger;
import biom4st3r.libs.moenchant_lib.ExtendedEnchantment;
import biom4st3r.libs.moenchant_lib.LibInit;
import net.fabricmc.api.ModInitializer;
import net.fabricmc.fabric.api.command.v2.CommandRegistrationCallback;
import net.fabricmc.fabric.api.itemgroup.v1.FabricItemGroup;
import net.fabricmc.fabric.api.itemgroup.v1.ItemGroupEvents;
import net.fabricmc.fabric.api.object.builder.v1.entity.FabricEntityTypeBuilder;
import net.fabricmc.loader.api.FabricLoader;
import net.minecraft.enchantment.Enchantment;
import net.minecraft.enchantment.EnchantmentLevelEntry;
import net.minecraft.entity.EntityDimensions;
import net.minecraft.entity.EntityType;
import net.minecraft.entity.SpawnGroup;
import net.minecraft.entity.attribute.EntityAttributes;
import net.minecraft.entity.mob.MobEntity;
import net.minecraft.item.EnchantedBookItem;
import net.minecraft.item.ItemGroup;
import net.minecraft.item.ItemStack;
import net.minecraft.item.Items;
import net.minecraft.registry.Registries;
import net.minecraft.registry.Registry;
import net.minecraft.util.Identifier;

public class ModInit implements ModInitializer {

    public static String moenchantments$oldIdentifier = "biom4st3rmoenchantments";
    public static final String MODID = "moenchantments".intern();
    public static final BioLogger logger = new BioLogger("Mo'Enchantments");
    public static boolean lockAutoSmeltSound = false;
    public static final boolean extraBowsFound = FabricLoader.getInstance().isModLoaded("extrabows");
    
    // TODO Fix enchantment tag.
    public static final ItemGroup enchantments = FabricItemGroup.builder(new Identifier(MODID,"enchantmentstab")) // FabricItemGroupBuilder
        // .create()
        .icon(() -> new ItemStack(Items.ENCHANTED_BOOK))
        .build()
        // .setEnchantments(ExtendedEnchantment.target)
        ;

    public static final EntityType<LivingItemEntity> LIVING_ITEM_TYPE = FabricEntityTypeBuilder
        .<LivingItemEntity>createLiving()
        .disableSummon()
        .entityFactory((type,world) -> new LivingItemEntity(world))
        .spawnGroup(SpawnGroup.MISC)
        .defaultAttributes(() -> MobEntity.createMobAttributes().add(EntityAttributes.GENERIC_MOVEMENT_SPEED, 0.3f))
        .trackRangeChunks(4)
        .dimensions(EntityDimensions.fixed(0.5F, 0.8F))
        .build();

    public static void test() {
        if (Boolean.FALSE);
    }

    @Override
    public void onInitialize() {
        LibInit.enableBetterAnvil = true;
        Registry.register(Registries.ENTITY_TYPE, new Identifier(ModInit.MODID, "livingitementity"), LIVING_ITEM_TYPE);
        EnchantmentRegistry.classLoad();
        EventCollection.init();
        CommandRegistrationCallback.EVENT.register(Commands.COMMANDS);
        ItemGroupEvents.modifyEntriesEvent(enchantments).register(content -> {
            for (Enchantment e : Registries.ENCHANTMENT) {
                ExtendedEnchantment.isExtended(e).ifPresent(ee -> {
                    content.add(EnchantedBookItem.forEnchantment(new EnchantmentLevelEntry(e, e.getMaxLevel())));
                });
            }
        });

        if (Plugin.NEW_FEATURES) NewFeatures.init();
    }
}