package com.biom4st3r.moenchantments.util;

import net.minecraft.server.network.ServerPlayerEntity;
import net.minecraft.server.world.ServerWorld;

import java.util.UUID;

import com.biom4st3r.moenchantments.entities.DummyServerNetworkHandler;
import com.mojang.authlib.GameProfile;

public class TestUtil {

    public static ServerPlayerEntity createServerPlayer(ServerWorld world) {
        ServerPlayerEntity spe = new ServerPlayerEntity(world.getServer(), world, new GameProfile(UUID.randomUUID(), "mock-server-player"));
        spe.networkHandler = new DummyServerNetworkHandler(world.getServer(), spe);
        return spe;
    }
}
