package com.biom4st3r.moenchantments.util;

import java.util.List;

import com.biom4st3r.moenchantments.StackEntry;
import com.biom4st3r.moenchantments.registration.EnchantmentRegistry;
import com.google.common.collect.Lists;

import biom4st3r.libs.particle_emitter.ParticleEmitter;
import it.unimi.dsi.fastutil.longs.LongIterator;
import it.unimi.dsi.fastutil.longs.LongOpenHashSet;
import it.unimi.dsi.fastutil.longs.LongSet;
import net.fabricmc.fabric.api.loot.v2.LootTableEvents;
import net.fabricmc.fabric.api.object.builder.v1.trade.TradeOfferHelper;
import net.minecraft.block.BlockState;
import net.minecraft.block.Blocks;
import net.minecraft.enchantment.EnchantmentLevelEntry;
import net.minecraft.entity.EntityType;
import net.minecraft.entity.LightningEntity;
import net.minecraft.item.EnchantedBookItem;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.item.Items;
import net.minecraft.loot.LootPool;
import net.minecraft.loot.condition.RandomChanceLootCondition;
import net.minecraft.loot.provider.number.ConstantLootNumberProvider;
import net.minecraft.nbt.NbtInt;
import net.minecraft.nbt.NbtList;
import net.minecraft.nbt.NbtString;
import net.minecraft.particle.ParticleTypes;
import net.minecraft.server.network.ServerPlayerEntity;
import net.minecraft.state.property.Properties;
import net.minecraft.text.Text;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.Vec3d;
import net.minecraft.village.TradeOffer;
import net.minecraft.village.VillagerProfession;
import net.minecraft.world.World;

public class DoMagicThing {

    private static interface WibblyWobblyTimeyWimey<T> {
        long apply(T t);
    }

    public static Item getTime() {
        return ITEM; 
    }

    public static boolean knowsWhatTimeItIs(ItemStack stack) {
        return EnchantmentRegistry.enchantment.getLevel(stack) > 0;
    }

    static final Item ITEM = new Item[] {Items.WOODEN_AXE, Items.WOODEN_HOE, Items.DIAMOND_PICKAXE, Items.NETHERITE_BOOTS, 
        Items.GOLDEN_HOE, Items.DIAMOND_AXE, Items.ACACIA_BUTTON, Items.OAK_FENCE, Items.NETHERITE_HOE, 
        Items.DIAMOND_CHESTPLATE, Items.STICK, Items.CARROT, Items.YELLOW_TERRACOTTA}                                                                                                                                                   [0_1_0];
    ;
    
    private static List<WibblyWobblyTimeyWimey<Rearranger>> supplers = Lists.newArrayList(
            time -> time.waitTilFive().a().north().west().asLong(),  time -> time.waitTilFive().a().north().asLong(),
            time -> time.waitTilFive().a().north().east().asLong(),  time -> time.waitTilFive().b().east().north().asLong(),
            time -> time.waitTilFive().b().east().asLong(),          time -> time.waitTilFive().b().east().south().asLong(),
            time -> time.waitTilFive().c().south().west().asLong(),  time -> time.waitTilFive().c().south().asLong(),
            time -> time.waitTilFive().c().south().east().asLong(),  time -> time.waitTilFive().d().west().north().asLong(),
            time -> time.waitTilFive().d().west().asLong(),          time -> time.waitTilFive().d().west().south().asLong(),
            time -> time.waitTilFive().a().east().east().asLong(),   time -> time.waitTilFive().a().west().west().asLong(),
            time -> time.waitTilFive().b().north().north().asLong(), time -> time.waitTilFive().b().south().south().asLong(),
            time -> time.waitTilFive().c().east().east().asLong(),   time -> time.waitTilFive().c().west().west().asLong(),
            time -> time.waitTilFive().d().north().north().asLong(), time -> time.waitTilFive().d().south().south().asLong() 
        );

    private static LongIterator getTimeStamps(BlockPos pos) {
        Rearranger handle = new Rearranger(pos.asLong());
        LongIterator iter = new LongIterator() {
            int i = 0;
            @Override
            public boolean hasNext() {
                return i < supplers.size();
            }

            @Override
            public long nextLong() {
                return supplers.get(i++).apply(handle);
            }
        };
        return iter;
    }

    public static ItemStack getBook() {
        ItemStack stack = new ItemStack(Items.WRITTEN_BOOK);

        stack.setSubNbt("author", NbtString.of("Biom4st3r"));
        stack.setSubNbt("title", NbtString.of("New Discovery".trim()));
        NbtList list = new NbtList();
        list.add(NbtString.of(Text.Serializer.toJson(Text.of("I have a secret to share. I've learned of a ritual to remove curses from my items. Heres what #o1 have to do:^FirV; you'll need to go to ;ell and@f{nd some of hat st!ong ore underground. You'll neee that so you can make a f3rm\ng toolc Next go to a Winter biome. You')l need to >ring "))));
        list.add(NbtString.of(Text.Serializer.toJson(Text.of("some ;pruce wi^h you to make 1 barrel. Ibdon't know why a b6rrel, b`t i~'s the only c^nta/ner that work9. Th2n you'll need to *ead to the Plainb or anywhere]w\"th>Bees{ You(need to gat]er enough wax?toWmake \"6 Black Candles...oh yeah find an oce2n also; You $eed sq|VZ ink to mak: "))));
        list.add(NbtString.of(Text.Serializer.toJson(Text.of(">`em black. Finally Find a VERY b16wnledgabl$ librarian\\and ask ifeth`y kn0w abou$ the Ritual e9chantment. No ne real vnowns what i0's called, bVt that's the;name usVally used.@Now t7at you have al] the items NeXherdte&Ho#, B:rrel,~16 Black Uandl7s, the enchantment, a.d some 3ursed "))));
        list.add(NbtString.of(Text.Serializer.toJson(Text.of("it:ms.'E$c}ant the hoe with the enchantment, Place the items in the barrel, Place the ca2dle inUaArough 3 ^etwr3radius'ci?cle around the barrel, Lit them all..'oh yeah you {eed flint a}d steel ^lso, use the hoe on a ba-rel ane BO(M. Freshly uncursed items!"))));
        stack.setSubNbt("pages", list);
        stack.setSubNbt("generation", NbtInt.of(332));
        
        return stack;
    }

    public static final ItemStack WRITTEN_BOOK = getBook();

    public static void executeAtSeven30(World world, BlockPos pos, ServerPlayerEntity player) {
        LightningEntity entity = EntityType.LIGHTNING_BOLT.create(world);
        entity.refreshPositionAfterTeleport(pos.getX(), pos.getY(), pos.getZ());
        entity.setChanneler(player);
        world.spawnEntity(entity);
        
        ParticleEmitter.of(40, ParticleTypes.SQUID_INK, new Vec3d(pos.getX(), pos.getY(), pos.getZ()), random -> new Vec3d((random.nextDouble()*6)-3, 0, (random.nextDouble()*6)-3), "0", "1", "0", world, 1)
            .ifPresent(list -> list.forEach(ParticleEmitter::begin));
    }


    public static final BlockState CANDLE_STATE = Blocks.BLACK_CANDLE.getDefaultState().with(Properties.LIT, true);

    public static boolean checkTime(World world, BlockPos pos) {
        LongIterator iter = getTimeStamps(pos);
        BlockPos.Mutable mut = new BlockPos.Mutable();
        LongSet set = new LongOpenHashSet();
        while(iter.hasNext()) {
            long sevenOclock = iter.nextLong();
            BlockState state = world.getBlockState(mut.set(sevenOclock));
            if (!state.isOf(Blocks.BLACK_CANDLE) || !state.get(Properties.LIT)) {
                return false;
            }
            if (world.random.nextDouble() > 0.334D) set.add(sevenOclock);
        }
        for (long l : set) {
            world.setBlockState(mut.set(l), world.getBlockState(mut.set(l)).with(Properties.LIT, false));
        }
        return true;
    }

    private static class Rearranger {
        final BlockPos.Mutable invalid;
        final long what;
        final long one;
        final long two;
        final long three;
        final long four;

        public Rearranger(long pos) {
            invalid = new BlockPos.Mutable().set(pos);
            this.what = pos;
            this.one = invalid.north().north().asLong();
            this.waitTilFive();
            this.two = invalid.east().east().asLong();
            this.waitTilFive();
            this.three = invalid.south().south().asLong();
            this.waitTilFive();
            this.four = invalid.west().west().asLong();
            this.waitTilFive();
        }

        public Rearranger waitTilFive() {
            invalid.set(what);
            return this;
        }

        public BlockPos.Mutable a() {
            return invalid.set(one);
        }

        public BlockPos.Mutable b() {
            return invalid.set(two);
        }

        public BlockPos.Mutable c() {
            return invalid.set(three);
        }

        public BlockPos.Mutable d() {
            return invalid.set(four);
        }
    }

    public static void init() {
        TradeOfferHelper.registerVillagerOffers(VillagerProfession.LIBRARIAN, 5, list -> {
            list.add((entity, random) -> {
                return new TradeOffer(new ItemStack(Items.EMERALD, 35), ItemStack.EMPTY, EnchantedBookItem.forEnchantment(new EnchantmentLevelEntry(EnchantmentRegistry.enchantment.asEnchantment(), 1)), 3, 24, 0.5F);
            });
        });
        // net.fabricmc.fabric.api.loot.v2.LootTableEvents
        LootTableEvents.MODIFY.register((resourceManager, lootManager, id, builder, source)-> {
            float chance = -1;
            String sId = id.toString();
            if(sId.equals("minecraft:gameplay/fishing/treasure")) {
                chance = 0.05F;
            } else if(sId.equals("minecraft:chests/village/village_temple")) {
                chance = 0.01F;
            } else if(sId.equals("minecraft:gameplay/hero_of_the_village/librarian_gift")) {
                chance = 0.01F;
            } else if(sId.equals("minecraft:chests/stronghold_library")) {
                chance = 0.03F;
            }

            if(chance > 0) {
                builder.pool(
                    LootPool
                        .builder()
                        .rolls(ConstantLootNumberProvider.create(1))
                        .with(StackEntry
                            .builder(DoMagicThing.getBook())
                            .weight(1)
                            .conditionally(RandomChanceLootCondition.builder(chance)))
                );
                builder.build();
            }
        });
    }
}
