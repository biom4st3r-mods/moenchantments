package com.biom4st3r.moenchantments.util;

import org.objectweb.asm.ClassWriter;
import org.objectweb.asm.Opcodes;
import org.objectweb.asm.Type;

import java.util.Random;
import java.util.function.Consumer;

import biom4st3r.libs.biow0rks.reflection.CtorRef;
import biom4st3r.libs.biow0rks.reflection.GodMode;

public class InPlaceAsm implements Opcodes {
    static Random random = new Random();
    public static <T> Consumer<T> start(Class<T> clazz, Consumer<OpcodeMethodVisitor> consumer) {
        ClassWriter cw = new ClassWriter(ClassWriter.COMPUTE_FRAMES | ClassWriter.COMPUTE_MAXS);
        OpcodeClassVisitor cv = OpcodeMethodVisitor.newCv(cw);
        String CLASS_NAME = "runner" + random.nextInt();
        cw.visit(V1_8, ACC_PUBLIC, CLASS_NAME, null, Type.getInternalName(Object.class), new String[]{Type.getInternalName(Consumer.class)});
        OpcodeMethodVisitor mv = cv.visitMethod(ACC_PUBLIC, "accept", "(Ljava/lang/Object;)V", null, null);
        
        mv.ALOAD(1).CHECKCAST(clazz);
        consumer.accept(mv);
        mv.FINISH(10, 10);
        
        cv.visitMethod(ACC_PUBLIC, "<init>", "()V", null, null)
            .ALOAD(0)
            .INVOKE_SPECIAL(Type.getInternalName(Object.class), "<init>", "()V")
            .RETURN()
            .FINISH(10, 10);
            ;
        try {
            Class<?> cl = GodMode.GOD.defineClass(cw.toByteArray());
            return (Consumer<T>) CtorRef.getCtor(cl, false, ctor -> true).newInstance();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
            throw new RuntimeException(e);
        }
    }
}
