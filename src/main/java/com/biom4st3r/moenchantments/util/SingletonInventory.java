package com.biom4st3r.moenchantments.util;

import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.inventory.Inventory;
import net.minecraft.item.ItemStack;

public final class SingletonInventory implements Inventory {

    private ItemStack stack = ItemStack.EMPTY;

    public SingletonInventory(ItemStack stack) {
        this.stack = stack;
    }
    public SingletonInventory() {
    }

    @Override
    public void clear() {
        this.stack = ItemStack.EMPTY;
    }

    @Override
    public boolean canPlayerUse(PlayerEntity player) {
        return true;
    }

    @Override
    public ItemStack getStack(int slot) {
        switch(slot) {
            case 0: return stack;
            default: return ItemStack.EMPTY;
        }
    }

    @Override
    public boolean isEmpty() {
        return this.stack.isEmpty();
    }

    @Override
    public void markDirty() {
    }

    @Override
    public ItemStack removeStack(int slot) {
        switch(slot) {
            case 0: 
                ItemStack is = stack;
                this.clear();
                return is;
            default: return ItemStack.EMPTY;
        }
    }

    @Override
    public ItemStack removeStack(int slot, int amount) {
        switch(slot) {
            case 0: this.stack.split(amount);
            default: return ItemStack.EMPTY;
        }
    }

    @Override
    public void setStack(int slot, ItemStack stack) {
        switch(slot) {
            case 0: 
                this.stack = stack;
                return;
            default: return;
        }
    }

    @Override
    public int size() {
        return 1;
    }
    
}
