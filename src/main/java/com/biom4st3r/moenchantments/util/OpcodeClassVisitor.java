package com.biom4st3r.moenchantments.util;

import org.objectweb.asm.ClassVisitor;
import org.objectweb.asm.Type;

public class OpcodeClassVisitor extends ClassVisitor {
    public OpcodeClassVisitor(ClassVisitor classVisitor) {
        super(OpcodeMethodVisitor.ASM_VERSION, classVisitor);
    }
    public OpcodeClassVisitor() {
        super(OpcodeMethodVisitor.ASM_VERSION);
    }
    
    public String superName;

    @Override
    public void visit(int version, int access, String name, String signature, String superName,
            String[] interfaces) {
        this.superName = superName;
        super.visit(OpcodeMethodVisitor.JAVA_VERSION, access, name, signature, superName, interfaces);
    }

    @Override
    public OpcodeMethodVisitor visitMethod(int access, String name, String descriptor, String signature,
            String[] exceptions) {
        return new OpcodeMethodVisitor(super.visitMethod(access, name, descriptor, signature, exceptions));
    }

    public OpcodeClassVisitor defaultCtor() {
        this.visitMethod(OpcodeMethodVisitor.ACC_PUBLIC, "<init>", "()V", null, null)
            .START()
            .ALOAD(0)
            .INVOKE_SPECIAL(this.superName, "<init>", "()V")
            .RETURN()
            .FINISH(5, 5);
        ;
        return this;
    }

    public static String methodDesc(Class<?> rt, Class<?>... args) {
        Type[] types = new Type[args.length];
        int i = 0;
        for(Class<?> c : args) {
            types[i++] = Type.getType(c);
        }
        return Type.getMethodDescriptor(Type.getType(rt), types);
    }

    public OpcodeMethodVisitor visitMethod(int access, String name, String sig, String[] exceptions, Class<?> rt, Class<?>... args) {
        return this.visitMethod(access, name, methodDesc(rt, args), sig, exceptions);
    }

    public OpcodeMethodVisitor visitMethod(int access, String name, Class<?> rt, Class<?>... args) {
        return this.visitMethod(access, name, methodDesc(rt, args), null, null);
    }
}