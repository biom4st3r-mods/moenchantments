package com.biom4st3r.moenchantments.entities;

import com.biom4st3r.moenchantments.ModInit;

import net.minecraft.util.Identifier;

public enum Emote {
    NONE("", 0xDEAD_BEEF),
    HAPPY("textures/emotions/happy.png", 0xFFea_b624),
    SAD("textures/emotions/sad.png", 0xFF2e_3a8c),
    VERY_HAPPY("textures/emotions/very_happy.png", 0xFF61_e27c),
    NEUTRAL("textures/emotions/neutral.png", 0xFF6e_6e6e),
    WAVE("textures/emotions/waving.png", 0xFFFF_FFFF),
    RAINBOW("textures/emotions/rainbow_flag.png", 0xFFFF_FFFF),
    TRANS("textures/emotions/trans_flag.png", 0xFFFF_FFFF),
    PANIC("textures/emotions/panic.png", 0xFFf9_3434),
    WEARY("textures/emotions/weary.png", 0xFFFF_9700)

    ;

    public Identifier identifier;
    public int color;

    Emote(String s, int color) {
        this.identifier = new Identifier(ModInit.MODID, s);
        this.color = color;
    }
}