package com.biom4st3r.moenchantments.entities;

import com.google.common.math.IntMath;

import biom4st3r.libs.biow0rks.BioLogger;
import net.minecraft.block.BlockState;
import net.minecraft.block.Blocks;
import net.minecraft.enchantment.EnchantmentHelper;
import net.minecraft.enchantment.Enchantments;
import net.minecraft.entity.ai.control.LookControl;
import net.minecraft.entity.ai.control.MoveControl;
import net.minecraft.entity.ai.goal.Goal;
import net.minecraft.entity.ai.pathing.EntityNavigation;
import net.minecraft.entity.ai.pathing.Path;
import net.minecraft.entity.effect.StatusEffectUtil;
import net.minecraft.item.ItemStack;
import net.minecraft.util.hit.BlockHitResult;
import net.minecraft.util.hit.HitResult.Type;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.Vec3d;
import net.minecraft.world.RaycastContext;
import net.minecraft.world.RaycastContext.FluidHandling;
import net.minecraft.world.RaycastContext.ShapeType;
import net.minecraft.world.World;

public class FindOreGoal2 extends Goal {
    
    private static final BioLogger LOGGER = new BioLogger("FIND_ORE_GOAL");

    private final LivingItemEntity entity;
    private final MoveControl movecontrol;
    private final EntityNavigation nav;
    private final LookControl look;
    private final Info info;

    private static final int MAX_BORED = 175;
    private static final int MAX_BLOCK_DAMAGE = 10;
    private static final double MIN_BREAK_DISTANCE = 4.0;
    private static final int NEW_NAV_THREASHOLD = (int)(MAX_BORED * (2f/3f));

    public final class Info {
        public Info() {
            this.init();
        }
        private void init() {
            this.setState(State.START);
            subtargetPos = BlockPos.ORIGIN;
            subtarget = Blocks.AIR.getDefaultState();
            ticksInCurrentState = 0;
            breakprogress = -1;
            this.path = null;
        }
        State CURR;
        BlockPos subtargetPos;
        BlockState subtarget;
        int ticksInCurrentState;
        int breakprogress;
        Path path;
        public void setState(State state) {
            this.ticksInCurrentState = 0;
            this.CURR = state;
        }
    }

    private enum State {
        START,
        MOVE,
        MINE,
        // MINE2,
        ;
    }

    /**
     * @param livingItemEntity
     */
    FindOreGoal2(LivingItemEntity livingItemEntity) {
        this.entity = livingItemEntity;
        this.movecontrol = this.entity.getMoveControl();
        this.nav = this.entity.getNavigation();
        this.info = new Info();
        this.look = this.entity.getLookControl();
    }

    private float getBreakTime(ItemStack stack, BlockState state, World world, BlockPos pos) {
        float hardness = state.getHardness(world, pos);
        if (hardness == -1.0f) {
            return 0.0f;
        }
        int i = 100; // player.canHarvest(state) ? 30 : 100;
        float toolBreakingSpeed = stack.getMiningSpeedMultiplier(state);
        if (toolBreakingSpeed > 1 && EnchantmentHelper.getLevel(Enchantments.EFFICIENCY, stack) > 0) {
            toolBreakingSpeed += IntMath.pow(EnchantmentHelper.getLevel(Enchantments.EFFICIENCY, stack), 2) + 1;
        }
        if (StatusEffectUtil.hasHaste(this.entity)) {
            toolBreakingSpeed *= (StatusEffectUtil.getHasteAmplifier(this.entity) +1) * 0.2F;
        }

        return toolBreakingSpeed / hardness / (float)i;
    }

    public boolean boredCheck() {
        if (this.info.ticksInCurrentState == NEW_NAV_THREASHOLD) {
            emote(Emote.NEUTRAL);
        }
        if (this.info.ticksInCurrentState > MAX_BORED) {
            if (info.CURR == State.MOVE) {
                this.nav.stop();
            }
            this.info.init();
            this.entity.info.init();
            LOGGER.debug("GOT BORED");
            emote(Emote.SAD);
            return true;
        } else {
            this.info.ticksInCurrentState++;
            return false;
        }
    }
    
    @Override
    public boolean canStart() {
        return entity.info.shouldMineOre && this.info.CURR == State.START;
    }

    @Override
    public void stop() {
        super.stop();
        this.info.init();
    }

    @Override
    public boolean shouldContinue() {
        return entity.info.shouldMineOre;
    }

    private BlockHitResult findSubtarget() {
        return this.entity.world.raycast(
            new RaycastContext(
                this.entity.getPos().add(0, this.entity.getStandingEyeHeight(), 0), 
                Vec3d.of(this.entity.info.targetOrePos).add(0.5, 3F/16F, 0.5), 
                ShapeType.OUTLINE, 
                FluidHandling.NONE, 
                entity
            ));
    }

    private boolean subtargetStillExists() {
        return this.entity.world.getBlockState(this.info.subtargetPos) == this.info.subtarget;
    }

    /**
     * Return false to skip tick
     * @param result
     * @return
     */
    private boolean populateInfo(BlockHitResult result) {
        if (result.getType() == Type.MISS) {
            this.info.init();
            this.entity.info.init();
            return false;
        }
        this.info.subtargetPos = result.getBlockPos();
        this.info.path = this.nav.findPathTo(result.getBlockPos(), 0);
        this.info.subtarget = this.entity.world.getBlockState(result.getBlockPos());
        return true;
    }
    private void emote(Emote emote) {
        this.entity.setEmote(emote);
    }


    @Override
    public void tick() {
        super.tick();
        if (boredCheck()) return;
        if (info.CURR != State.START && !subtargetStillExists()) {
            this.info.init();
            return;
        }

        if (info.CURR == State.START) {
            LOGGER.debug("Starting");
            BlockHitResult result = findSubtarget();
            if (!populateInfo(result)) {
                return;
            }
            LOGGER.debug("Raycast hit: %s", result.getBlockPos());
            look.lookAt(Vec3d.of(this.info.subtargetPos));

            this.info.setState(State.MOVE);
        } else if (info.CURR == State.MOVE) {
            double distance = info.subtargetPos.getSquaredDistance(this.entity.getPos());
            if (distance > 500) {
                this.info.init();
                this.entity.info.init();
                return;
            }
            LOGGER.debug("Distinace to subtarget: %s", distance);
            if (distance <= MIN_BREAK_DISTANCE) {
                this.nav.stop();
                this.info.setState(State.MINE);
            } else if (this.info.ticksInCurrentState < NEW_NAV_THREASHOLD) {
                this.nav.startMovingAlong(this.info.path, 1.0F);
            } else {
                this.movecontrol.moveTo(this.info.subtargetPos.getX(), this.info.subtargetPos.getY(), this.info.subtargetPos.getZ(), 1.0F);
            }
        } else if (info.CURR == State.MINE) {
            LOGGER.debug("Breaking!");
            if (this.info.breakprogress % 4 == 0) {
                look.lookAt(this.info.subtargetPos.getX(), this.info.subtargetPos.getY()-1, this.info.subtargetPos.getZ());

            } else if (this.info.breakprogress % 6 == 0) {
                look.lookAt(this.info.subtargetPos.getX(), this.info.subtargetPos.getY(), this.info.subtargetPos.getZ());
            }
            this.info.breakprogress++;
            float breakmul = getBreakTime(this.entity.getStack(), this.info.subtarget, this.entity.world, this.info.subtargetPos);
            breakmul *= 40;

            float current_progress = this.info.breakprogress * breakmul;
            this.entity.world.setBlockBreakingInfo(this.entity.getId(), this.info.subtargetPos, Math.min((int)(current_progress), 10));
            if (Float.isNaN(current_progress) || Float.isInfinite(current_progress)) current_progress = MAX_BLOCK_DAMAGE + 1;

            LOGGER.debug("Break progress: %s / %s", this.info.breakprogress * breakmul, MAX_BLOCK_DAMAGE);
            if (current_progress < MAX_BLOCK_DAMAGE ) return;
            this.entity.world.setBlockBreakingInfo(this.entity.getId(), this.info.subtargetPos, -1);
            this.entity.breakBlockAsPlayer(this.info.subtargetPos);
            if (this.entity.info.targetOrePos.equals(this.info.subtargetPos)) {
                this.entity.info.init();
                LOGGER.debug("Broke target, small re-searching");
                this.entity.searchForOre(2, 2, 2);
            }
            this.info.init();
        }
        // raycast at the block
        // pathfind to the hit
        // break
        // repeat
    }
    
}
