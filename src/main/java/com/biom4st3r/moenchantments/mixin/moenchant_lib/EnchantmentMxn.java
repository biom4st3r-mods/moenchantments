package com.biom4st3r.moenchantments.mixin.moenchant_lib;

import net.minecraft.enchantment.Enchantment;

import biom4st3r.libs.moenchant_lib.ExtendedEnchantment;
import org.spongepowered.asm.mixin.Mixin;

@Mixin({Enchantment.class})
public abstract class EnchantmentMxn implements ExtendedEnchantment {
    @Override
    public boolean isExtended() {
        return false;
    }
}
