package com.biom4st3r.moenchantments.mixin.filterfeeder;

import org.spongepowered.asm.mixin.Mixin;

import net.minecraft.item.Item;


@Mixin({Item.class})
public class ItemMxn {
    /**
     * @whyhere minecraft used to check if items were registered yet i think.
     */
    // Don't look like i need this anymore
    // @Redirect(method = "<init>*", at = @At(value = "INVOKE", 
    //     target = "net/minecraft/util/registry/DefaultedRegistry.createEntry(Ljava/lang/Object;)Lnet/minecraft/util/registry/RegistryEntry$Reference;"))
    // private Reference<Item> donotCreateEntry(SimpleDefaultedRegistry<Item> registry, Object item) {
    //     if (((Object)item) instanceof DUMMYFOODITEM) {
    //         return (Reference<Item>) ItemStack.EMPTY.getRegistryEntry();
    //     }
    //     // Registry.ITEM.createEntry(value)
    //     return registry.createEntry((Item) item);
    // }
}
