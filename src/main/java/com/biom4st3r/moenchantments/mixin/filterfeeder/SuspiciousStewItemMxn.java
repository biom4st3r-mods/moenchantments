package com.biom4st3r.moenchantments.mixin.filterfeeder;

import java.util.function.Consumer;

import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.Shadow;
import org.spongepowered.asm.mixin.injection.At;
import org.spongepowered.asm.mixin.injection.Inject;
import org.spongepowered.asm.mixin.injection.callback.CallbackInfoReturnable;

import com.biom4st3r.moenchantments.logic.FilterFeeder;

import net.minecraft.entity.LivingEntity;
import net.minecraft.entity.effect.StatusEffectInstance;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.item.Items;
import net.minecraft.item.SuspiciousStewItem;
import net.minecraft.world.World;

@Mixin({SuspiciousStewItem.class})
public abstract class SuspiciousStewItemMxn extends Item {

    public SuspiciousStewItemMxn(Settings settings) {
        super(settings);
    }

    @Shadow
    private static void forEachEffect(ItemStack stew, Consumer<StatusEffectInstance> effectConsumer) {return;}

    // @Redirect(
    //     method = "finishUsing",
    //     at = @At(
    //         value = "INVOKE", 
    //         target = "net/minecraft/entity/LivingEntity.addStatusEffect(Lnet/minecraft/entity/effect/StatusEffectInstance;)Z",
    //         ordinal = 0))
    // private boolean replaceSusStewItemForFilterFeeder(LivingEntity entity, StatusEffectInstance instance) {
    //     if (FilterFeeder.hasValidItem(entity)) {
    //         return entity.addStatusEffect(FilterFeeder.getInverse(instance));
    //     } else {
    //         return entity.addStatusEffect(instance);
    //     }
    // }
    @Inject(
        method = "finishUsing",
        at = @At(
            value = "HEAD"
        ),
        cancellable = true
    )
    private void moenchantments$finishusing(ItemStack stack, World world, LivingEntity entity, CallbackInfoReturnable<ItemStack> ci) {
        if (FilterFeeder.hasValidItem(entity)) {
            final ItemStack is = super.finishUsing(stack,world,entity);
            forEachEffect(is, (effect)-> {
                entity.addStatusEffect(FilterFeeder.getInverse(effect));
            });
            ci.setReturnValue(entity instanceof PlayerEntity pe && pe.getAbilities().creativeMode ? is : new ItemStack(Items.BOWL));
            ci.cancel();
            return;
        }
    }
}
