package com.biom4st3r.moenchantments.mixin.notadatafixer;

import org.spongepowered.asm.mixin.Mixin;

import net.minecraft.util.Identifier;

@Mixin({Identifier.class})
public class Identifiermxn {
    // I probably don't need this anymore.
    // @Inject(
    //     at = @At(
    //         value = "INVOKE", 
    //         target = "java/lang/Object.<init>()V", 
    //         shift = Shift.AFTER), 
    //     method = "<init>(Ljava/lang/String;Ljava/lang/String;Lnet/minecraft/util/Identifier/ExtraData;)V")
    // private void moenchantments_fix_id(String namespace, String path, Identifier.ExtraData extraData, CallbackInfo ci) {
    //     if ("biom4st3rmoenchantments".equals(strings[0])) {
    //         System.out.println("\n\n\nUSE OF OLD MOENCHANTMENTS ID\n\n\n");
    //         new Exception().printStackTrace();
    //         strings[0] = ModInit.MODID;
    //     }
    // }
}
