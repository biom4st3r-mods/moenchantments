package com.biom4st3r.moenchantments.mixin.compat.architectury.lifelike;

import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.injection.At;
import org.spongepowered.asm.mixin.injection.At.Shift;
import org.spongepowered.asm.mixin.injection.Inject;
import org.spongepowered.asm.mixin.injection.callback.CallbackInfoReturnable;
import org.spongepowered.asm.mixin.injection.callback.LocalCapture;

import com.biom4st3r.moenchantments.entities.LivingItemEntity;
import com.biom4st3r.moenchantments.logic.LivingItemLogic;
import com.biom4st3r.moenchantments.registration.EnchantmentRegistry;

import net.minecraft.block.BlockState;
import net.minecraft.entity.mob.PatrolEntity;
import net.minecraft.server.world.ServerWorld;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.random.Random;
import net.minecraft.world.spawner.PatrolSpawner;

@Mixin({PatrolSpawner.class})
public class PatrolSpawnerMixin {
    /**
     * @whyhere Refer to lifelike/PatrolSpawnMixin. 
     * This specific mixin to because localcapture here from architectury changes the LVT by adding a BlockState
     */
    @Inject(
        at = @At(
            value = "INVOKE", 
            target = "net/minecraft/server/world/ServerWorld.spawnEntityAndPassengers(Lnet/minecraft/entity/Entity;)V", 
            shift = Shift.AFTER),
        method = "spawnPillager",
        locals = LocalCapture.CAPTURE_FAILHARD
    )
    private void alsoSpawnLivingItemEntity(
        ServerWorld world, BlockPos pos, Random random, 
        boolean captain, CallbackInfoReturnable<Boolean> ci, BlockState state, PatrolEntity patrolEntity
    ) {
        if (!EnchantmentRegistry.LIFE_LIKE.isEnabled()) return;
        if (ci.getReturnValueZ() && captain && world.getRandom().nextInt(LivingItemEntity.SPAWN_WITH_PATROL_CHANCE) == 0) {
            LivingItemLogic.spawnWithPillager(world, patrolEntity);
        }
    }

}
