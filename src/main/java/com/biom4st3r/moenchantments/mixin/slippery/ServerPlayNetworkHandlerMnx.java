package com.biom4st3r.moenchantments.mixin.slippery;

import net.minecraft.network.packet.c2s.play.UpdateSelectedSlotC2SPacket;
import net.minecraft.server.network.ServerPlayNetworkHandler;
import net.minecraft.server.network.ServerPlayerEntity;

import com.biom4st3r.moenchantments.logic.Slippery;
import com.biom4st3r.moenchantments.registration.EnchantmentRegistry;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.Shadow;
import org.spongepowered.asm.mixin.injection.At;
import org.spongepowered.asm.mixin.injection.Inject;
import org.spongepowered.asm.mixin.injection.callback.CallbackInfo;

@Mixin({ServerPlayNetworkHandler.class})
public class ServerPlayNetworkHandlerMnx {

    @Shadow public ServerPlayerEntity player;
    /**
     * @whyhere Tail of an if block that checks if that action was valid.
     */
    @Inject(
        method = "onUpdateSelectedSlot", 
        at = @At(
            value = "INVOKE",
            target = "net/minecraft/server/network/ServerPlayerEntity.updateLastActionTime()V")
        )
    private void moEnchantments_onUpdateSlotTrySlippery(UpdateSelectedSlotC2SPacket packet, CallbackInfo ci) {
        // I don't actually know when this gets called, but that's okay.
        if (EnchantmentRegistry.SLIPPERY.hasEnchantment(player.getMainHandStack())) {
            if (Slippery.shouldDropOnSwap(player.getRandom())) {
                Slippery.doDrop(player);
            }
        }
    }
}
