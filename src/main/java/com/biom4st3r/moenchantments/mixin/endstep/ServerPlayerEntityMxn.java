package com.biom4st3r.moenchantments.mixin.endstep;

import java.util.Optional;

import net.minecraft.server.network.ServerPlayerEntity;

import com.biom4st3r.moenchantments.interfaces.EndStepper;
import com.biom4st3r.moenchantments.logic.EndStepProgressCounter;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.injection.At;
import org.spongepowered.asm.mixin.injection.Inject;
import org.spongepowered.asm.mixin.injection.callback.CallbackInfo;
import org.spongepowered.asm.mixin.injection.callback.LocalCapture;

@Mixin({ServerPlayerEntity.class})
public class ServerPlayerEntityMxn {
    /**
     * @whyhere any place at the top is fine we just need to make the entity un-teleportable
     */
    @Inject(
        at = @At("HEAD"), 
        method = "requestTeleport", 
        cancellable = true,
        locals = LocalCapture.NO_CAPTURE)
    private void moenchantment$preventTeleportDuringEndStep(double destX, double destY, double destZ, CallbackInfo ci) {
        if (this instanceof EndStepper THIS) {
            Optional<EndStepProgressCounter> i = THIS.moenchantment$getStepProgress();
            if (i.isPresent() && !i.get().canTeleport()) {
                ci.cancel();
            }
        }
    }
}
