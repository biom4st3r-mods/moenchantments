package com.biom4st3r.moenchantments.mixin.endstep;

import java.util.Optional;

import com.biom4st3r.moenchantments.interfaces.EndStepper;
import com.biom4st3r.moenchantments.logic.EndStepProgressCounter;

import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.Unique;
import org.spongepowered.asm.mixin.injection.At;
import org.spongepowered.asm.mixin.injection.Inject;
import org.spongepowered.asm.mixin.injection.callback.CallbackInfo;
import org.spongepowered.asm.mixin.injection.callback.LocalCapture;

import net.minecraft.entity.LivingEntity;

@Mixin({LivingEntity.class})
public class LivingEntityMixin implements EndStepper {
    
    @Unique
    private Optional<EndStepProgressCounter> moenchantment$endstepcounter = Optional.empty();

    @Override
    public void moenchantment$setStepProgress(Optional<EndStepProgressCounter> optional) {
        this.moenchantment$endstepcounter = optional;
    }
    /**
     * @whyhere any place at the top is fine we just need to make the entity immoble
     */
    @Inject(
        at = @At("HEAD"), 
        method = "tickMovement", 
        cancellable = true,
        locals = LocalCapture.NO_CAPTURE)
    private void stopMovementForEndStep(CallbackInfo ci) {
        if (moenchantment$endstepcounter.isPresent()) {
            moenchantment$endstepcounter.get().tick();
            ci.cancel();
        }
    }
    /**
     * @whyhere any place at the top is fine we just need to make the entity immoble
     */
    @Inject(
        at = @At("HEAD"), 
        method = "jump", 
        cancellable = true,
        locals = LocalCapture.NO_CAPTURE)
    private void stopJumpingForEndStep(CallbackInfo ci) {
        if (moenchantment$endstepcounter.isPresent()) {
            moenchantment$endstepcounter.get().tick();
            ci.cancel();
        }
    }

    @Override
    public Optional<EndStepProgressCounter> moenchantment$getStepProgress() {
        return moenchantment$endstepcounter;
    }
}
