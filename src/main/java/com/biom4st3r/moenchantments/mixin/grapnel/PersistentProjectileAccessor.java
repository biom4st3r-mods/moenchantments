package com.biom4st3r.moenchantments.mixin.grapnel;

import net.minecraft.entity.projectile.PersistentProjectileEntity;

import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.gen.Accessor;

@Mixin({PersistentProjectileEntity.class})
public interface PersistentProjectileAccessor {
    @Accessor("inGround")
    boolean isInGround();
}
