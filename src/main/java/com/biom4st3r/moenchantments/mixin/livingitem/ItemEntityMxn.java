package com.biom4st3r.moenchantments.mixin.livingitem;

import com.biom4st3r.moenchantments.entities.LivingItemEntity;
import com.biom4st3r.moenchantments.registration.EnchantmentRegistry;

import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.Shadow;
import org.spongepowered.asm.mixin.injection.At;
import org.spongepowered.asm.mixin.injection.Inject;
import org.spongepowered.asm.mixin.injection.callback.CallbackInfo;

import net.minecraft.enchantment.EnchantmentHelper;
import net.minecraft.entity.Entity;
import net.minecraft.entity.EntityType;
import net.minecraft.entity.ItemEntity;
import net.minecraft.item.ItemStack;
import net.minecraft.world.World;

@Mixin({ItemEntity.class})
public abstract class ItemEntityMxn extends Entity {
    public ItemEntityMxn(EntityType<?> type, World world) {
        super(type, world);
    }
    /**
     * @whyhere before anything can happen to the item entity it'll be transformed into a living item
     */
    @Inject(method = "tick",at = @At("HEAD"))
    private void moenchantments$removeOnTickIfEnchanted(CallbackInfo ci) {
        if (this.world.isClient) return;
        if (!EnchantmentRegistry.LIFE_LIKE.isEnabled()) return;
        if (EnchantmentHelper.getLevel(EnchantmentRegistry.LIFE_LIKE.asEnchantment(), getStack()) > 0) {
            Entity e = LivingItemEntity.create((ItemEntity)(Object)this);
            if (e != null) {
                this.remove(RemovalReason.DISCARDED);
                this.world.spawnEntity(e);
            }
        }
    }

    @Shadow
    public abstract ItemStack getStack();
}
