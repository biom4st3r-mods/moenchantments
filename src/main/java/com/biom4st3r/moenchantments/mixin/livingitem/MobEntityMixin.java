package com.biom4st3r.moenchantments.mixin.livingitem;

import org.jetbrains.annotations.Nullable;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.injection.At;
import org.spongepowered.asm.mixin.injection.Inject;
import org.spongepowered.asm.mixin.injection.callback.CallbackInfoReturnable;

import com.biom4st3r.moenchantments.entities.LivingItemEntity;
import com.biom4st3r.moenchantments.logic.LivingItemLogic;

import net.minecraft.entity.EntityData;
import net.minecraft.entity.EntityType;
import net.minecraft.entity.LivingEntity;
import net.minecraft.entity.SpawnReason;
import net.minecraft.entity.mob.MobEntity;
import net.minecraft.entity.passive.VillagerEntity;
import net.minecraft.nbt.NbtCompound;
import net.minecraft.world.LocalDifficulty;
import net.minecraft.world.ServerWorldAccess;
import net.minecraft.world.World;

@Mixin({MobEntity.class})
public abstract class MobEntityMixin extends LivingEntity {
    protected MobEntityMixin(EntityType<? extends LivingEntity> entityType, World world) {
        super(entityType, world);
    }

    /**
     * @why when a villager is spawned, because of a structure they have a chance of owning a living item
     * this is kind of hacky, but i like it :shrug:
     */
    @Inject(at = @At("RETURN"), method = "initialize")
    private void onInitlizeMaybeSpawnLIE(ServerWorldAccess world, LocalDifficulty difficulty, SpawnReason spawnReason, @Nullable EntityData entityData, @Nullable NbtCompound entityNbt, CallbackInfoReturnable<EntityData> ci) {
        if ((Object)this instanceof VillagerEntity ve && spawnReason == SpawnReason.STRUCTURE && world.getRandom().nextInt(LivingItemEntity.SPAWN_WITH_VILLAGER_CHANCE) == 0) {
            LivingItemLogic.spawnWithVillager(world, ve);
        }
    }
}
