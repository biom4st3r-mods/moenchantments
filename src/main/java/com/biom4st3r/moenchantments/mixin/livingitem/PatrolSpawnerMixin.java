package com.biom4st3r.moenchantments.mixin.livingitem;

import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.injection.At;
import org.spongepowered.asm.mixin.injection.Inject;
import org.spongepowered.asm.mixin.injection.callback.CallbackInfoReturnable;
import org.spongepowered.asm.mixin.injection.callback.LocalCapture;

import com.biom4st3r.moenchantments.entities.LivingItemEntity;
import com.biom4st3r.moenchantments.logic.LivingItemLogic;
import com.biom4st3r.moenchantments.registration.EnchantmentRegistry;

import net.minecraft.entity.mob.PatrolEntity;
import net.minecraft.server.world.ServerWorld;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.random.Random;
import net.minecraft.world.spawner.PatrolSpawner;

@Mixin({PatrolSpawner.class})
public class PatrolSpawnerMixin {
    @Inject(
        at = @At(value = "RETURN", ordinal = 2), 
        method = "spawnPillager",
        locals = LocalCapture.CAPTURE_FAILHARD
    )
    private void alsoSpawnLivingItemEntity(
        ServerWorld world, BlockPos pos, Random random, 
        boolean captain, CallbackInfoReturnable<Boolean> ci, PatrolEntity patrolEntity
    ) {
        if (!EnchantmentRegistry.LIFE_LIKE.isEnabled()) return;
        if (ci.getReturnValueZ() && captain && world.getRandom().nextInt(LivingItemEntity.SPAWN_WITH_PATROL_CHANCE) == 0) {
            LivingItemLogic.spawnWithPillager(world, patrolEntity);
        }
    }
}
