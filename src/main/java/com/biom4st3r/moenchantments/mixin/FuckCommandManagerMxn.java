package com.biom4st3r.moenchantments.mixin;

import java.io.ByteArrayOutputStream;
import java.io.PrintWriter;

import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.injection.At;
import org.spongepowered.asm.mixin.injection.At.Shift;
import org.spongepowered.asm.mixin.injection.Inject;
import org.spongepowered.asm.mixin.injection.callback.CallbackInfoReturnable;
import org.spongepowered.asm.mixin.injection.callback.LocalCapture;

import com.biom4st3r.moenchantments.ModInit;
import com.mojang.brigadier.ParseResults;

import net.minecraft.server.command.CommandManager;
import net.minecraft.server.command.ServerCommandSource;
import net.minecraft.text.MutableText;

/**
 * Prints error from commands. only enabled in dev
 */
@Mixin({CommandManager.class})
public class FuckCommandManagerMxn {
    @Inject(
       at = @At(
          value = "INVOKE",
          target = "org/slf4j/Logger.isDebugEnabled()Z",
          ordinal = -1,
          shift = Shift.BEFORE),
       method = "execute",
       cancellable = false,
       locals = LocalCapture.CAPTURE_FAILHARD)
    private void asdf(ParseResults<ServerCommandSource> parseResults, String command, CallbackInfoReturnable<Integer> cir, ServerCommandSource scs, Exception e, MutableText mt) {
        ByteArrayOutputStream strema = new ByteArrayOutputStream();
        PrintWriter writer = new PrintWriter(strema);
        e.printStackTrace(writer);
        writer.flush();
        ModInit.logger.error(strema.toString());
    }
}

