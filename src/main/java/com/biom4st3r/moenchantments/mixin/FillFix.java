package com.biom4st3r.moenchantments.mixin;

import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.injection.Constant;
import org.spongepowered.asm.mixin.injection.ModifyConstant;

import net.minecraft.server.command.FillCommand;

@Mixin(FillCommand.class)
public abstract class FillFix {
    @ModifyConstant(
        method = "execute", 
        constant = @Constant(
            intValue = 32768)
        )
    private static int fixeMe(int i) {
        return Integer.MAX_VALUE;
    }
}