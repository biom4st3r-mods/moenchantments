package com.biom4st3r.moenchantments.mixin.potion_retention;

import java.util.Map;

import net.minecraft.entity.Entity;
import net.minecraft.entity.LivingEntity;
import net.minecraft.entity.effect.StatusEffect;
import net.minecraft.entity.effect.StatusEffectInstance;

import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.gen.Accessor;
import org.spongepowered.asm.mixin.gen.Invoker;

@Mixin({LivingEntity.class})
public interface PotionRetentionAccessor {
    @Invoker("onStatusEffectApplied")
    void invokeOnStatusEffectApplied(StatusEffectInstance instance, Entity source);
    @Accessor("activeStatusEffects")
    Map<StatusEffect, StatusEffectInstance> getActiveStatusEffects();
    @Invoker("canHaveStatusEffect")
    boolean invokeCanHaveStatusEffect(StatusEffectInstance instance);
}
