package com.biom4st3r.moenchantments.registration;

import java.util.function.Predicate;

import net.minecraft.block.Blocks;
import net.minecraft.entity.EquipmentSlot;
import net.minecraft.item.ArmorItem;
import net.minecraft.item.BowItem;
import net.minecraft.item.CrossbowItem;
import net.minecraft.item.ItemStack;
import net.minecraft.item.MiningToolItem;
import net.minecraft.item.SwordItem;
import net.minecraft.item.ToolItem;

public interface EnchantmentPresets {
    // static FieldHandler<TagKey<Block>> MiningToolItem$effectiveBlocks = FieldHandler.get(MiningToolItem.class, f -> f.getType() == TagKey.class);

    public static final EquipmentSlot[] 
        ARMOR_SLOTS = new EquipmentSlot[] {EquipmentSlot.CHEST, EquipmentSlot.FEET, EquipmentSlot.HEAD, EquipmentSlot.LEGS},
        ALL_SLOTS = new EquipmentSlot[] {EquipmentSlot.CHEST, EquipmentSlot.FEET, EquipmentSlot.HEAD, EquipmentSlot.LEGS, EquipmentSlot.MAINHAND, EquipmentSlot.OFFHAND},
        HAND_SLOTS = new EquipmentSlot[] {EquipmentSlot.MAINHAND, EquipmentSlot.OFFHAND};

    static Predicate<ItemStack> 
        isArmor = stack -> stack.getItem() instanceof ArmorItem,
        isChest = stack -> stack.getItem() instanceof ArmorItem armor && armor.getSlotType() == EquipmentSlot.CHEST,
        isShoe =  stack -> stack.getItem() instanceof ArmorItem armor && armor.getSlotType() == EquipmentSlot.FEET,
        isPant =  stack -> stack.getItem() instanceof ArmorItem armor && armor.getSlotType() == EquipmentSlot.LEGS,
        isHat =   stack -> stack.getItem() instanceof ArmorItem armor && armor.getSlotType() == EquipmentSlot.HEAD,
        isDamageableNotArmor = stack -> stack.getItem().isDamageable() && !isArmor.test(stack),
        isMiningTool = stack -> stack.getItem() instanceof MiningToolItem,
        isTool = stack -> stack.getItem() instanceof ToolItem,
        isPickaxe = stack -> stack.getItem().isSuitableFor(Blocks.STONE.getDefaultState()),
        isAxe = stack -> stack.getItem().isSuitableFor(Blocks.SPRUCE_LOG.getDefaultState()),
        isSword = stack -> stack.getItem() instanceof SwordItem,
        YES = stack -> true,
        NO = YES.negate(),
        isBow = stack -> stack.getItem() instanceof BowItem,
        isCrossbow = stack -> stack.getItem() instanceof CrossbowItem
    ;
}
