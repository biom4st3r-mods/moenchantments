package com.biom4st3r.moenchantments.registration;

import java.util.Collections;
import java.util.List;
import java.util.concurrent.TimeUnit;
import java.util.stream.Stream;

import com.biom4st3r.moenchantments.ModInit;
import com.biom4st3r.moenchantments.entities.Emote;
import com.biom4st3r.moenchantments.entities.LivingItemEntity;
import com.biom4st3r.moenchantments.logic.RetainedPotion;
import com.biom4st3r.moenchantments.util.DoMagicThing;
import com.biom4st3r.moenchantments.util.LootHelper;
import com.biom4st3r.moenchantments.util.TagHelper;
import com.google.common.base.Stopwatch;
import com.google.common.collect.Lists;
import com.mojang.brigadier.CommandDispatcher;
import com.mojang.brigadier.arguments.BoolArgumentType;
import com.mojang.brigadier.arguments.IntegerArgumentType;
import com.mojang.brigadier.arguments.StringArgumentType;
import com.mojang.brigadier.builder.LiteralArgumentBuilder;

import net.fabricmc.fabric.api.command.v2.CommandRegistrationCallback;
import net.fabricmc.loader.api.FabricLoader;
import net.minecraft.command.CommandRegistryAccess;
import net.minecraft.command.CommandSource;
import net.minecraft.command.EntitySelector;
import net.minecraft.command.argument.EntityArgumentType;
import net.minecraft.command.argument.IdentifierArgumentType;
import net.minecraft.enchantment.Enchantment;
import net.minecraft.enchantment.EnchantmentHelper;
import net.minecraft.entity.Entity;
import net.minecraft.entity.EquipmentSlot;
import net.minecraft.entity.LivingEntity;
import net.minecraft.entity.SpawnReason;
import net.minecraft.entity.mob.MobEntity;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.item.Items;
import net.minecraft.loot.context.LootContextTypes;
import net.minecraft.loot.entry.LootPoolEntry;
import net.minecraft.loot.function.EnchantRandomlyLootFunction;
import net.minecraft.loot.function.EnchantWithLevelsLootFunction;
import net.minecraft.registry.Registries;
import net.minecraft.registry.tag.TagKey;
import net.minecraft.server.command.CommandManager;
import net.minecraft.server.command.CommandManager.RegistrationEnvironment;
import net.minecraft.server.command.ServerCommandSource;
import net.minecraft.server.network.ServerPlayerEntity;
import net.minecraft.server.world.ServerWorld;
import net.minecraft.text.Text;
import net.minecraft.util.Identifier;
import net.minecraft.util.math.random.Random;
import net.minecraft.world.Difficulty;
import net.minecraft.world.LocalDifficulty;
import net.minecraft.world.spawner.PatrolSpawner;

public class Commands implements CommandRegistrationCallback  {

    private Commands() {
    }

    public static final CommandRegistrationCallback COMMANDS = new Commands();

    @Override
    public void register(CommandDispatcher<ServerCommandSource> dispatcher, CommandRegistryAccess registryAccess,
            RegistrationEnvironment environment) {
        
        LiteralArgumentBuilder<ServerCommandSource> ROOT = CommandManager
            .literal("moenchantments")
            .requires(s -> s.getEntity() instanceof ServerPlayerEntity && s.getEntity().hasPermissionLevel(2))
            .then(LIFELIKE
            )
            .then(moenchantments)
            .then(test_book)
            .then(moenchantmentsItems)
            .then(PATROL_SPAWN)
            .then(RT_POOL_SIZE)
            .then(INIT_MOB_TILL_ENCHANT)
            ;
            
        ;
        dispatcher.register(ROOT);
    }

    static enum SuggestTag {
        ARMORS,
        AXES,
        BOOTS,
        BOWS,
        CHESTPLATES,
        EVERYTHING,
        HELMS,
        LEGGINGS,
        PICKAXES,
        SHOVELS,
        SWORDS,
        TOOLS,
        WEAPONS,
        ;

        public final TagKey<Item> tag;

        SuggestTag() {
            this.tag = TagHelper.item(new Identifier(ModInit.MODID, this.name().toLowerCase()));
            // this.tag = TagFactory.ITEM.create(new Identifier(ModInit.MODID, this.name().toLowerCase()));
        }

        public static Stream<String> strings() {
            return Stream.of(SuggestTag.values()).map(t -> t.name());
        }
    }

    static enum SuggestEnchantmentType {
        ENCHANT_RANDOMLY,
        ENCHANT_WITH_LEVELS
    }
    
    static LiteralArgumentBuilder<ServerCommandSource> INIT_MOB_TILL_ENCHANT = CommandManager
        .literal("init_mob_till_weapon_enchant")
        .then(
            CommandManager.argument("entity", EntityArgumentType.entity())
                .executes(ctx -> {
                    Entity e = ctx.getArgument("entity", EntitySelector.class).getEntity(ctx.getSource());
                    if (e instanceof MobEntity me) {
                        int i = 0;
                        me.equipStack(EquipmentSlot.MAINHAND, ItemStack.EMPTY);
                        while (true) {
                            LocalDifficulty ld = new LocalDifficulty(Difficulty.HARD, 1000, 100000, 2);
                            //e.world.getLocalDifficulty(e.getBlockPos())
                            me.initialize((ServerWorld)e.getWorld(), ld, SpawnReason.NATURAL, null, null);
                            boolean haspr = EnchantmentRegistry.POTIONRETENTION.hasEnchantment(me.getEquippedStack(EquipmentSlot.MAINHAND));
                            // boolean hasany = me.getEquippedStack(EquipmentSlot.MAINHAND).hasEnchantments();
                            if (haspr) {
                                for (Enchantment enchant : EnchantmentHelper.get(me.getEquippedStack(EquipmentSlot.MAINHAND)).keySet())
                                    ctx.getSource().sendFeedback(enchant.getName(1), false);
                                return 1;
                            }
                            i++;
                            if (i > 500) break;
                        }
                        ctx.getSource().sendFeedback(Text.literal("Failed"), false);
                    }
                    return 1;
                })
        )
        ;
    
    static LiteralArgumentBuilder<ServerCommandSource> RT_POOL_SIZE = CommandManager
        .literal("rt_pool_size")
        .executes(ctx -> {
            ctx.getSource().sendFeedback(Text.literal(RetainedPotion.debug_getRtPoolSize() + " RetainedPotion Objects Active."), false);
            return 1;
           });

    static LiteralArgumentBuilder<ServerCommandSource> PATROL_SPAWN = CommandManager
        .literal("spawn_patrol")
        .executes(ctx -> {
            PatrolSpawner spawner = new PatrolSpawner();
            
            ctx.getSource().sendFeedback(Text.literal("Spawned: " + spawner.spawn(ctx.getSource().getWorld(), true, true)), false);
            return 1;
        });

    static LiteralArgumentBuilder<ServerCommandSource> LIFELIKE = CommandManager
        .literal("lifelike")
        .requires(s -> FabricLoader.getInstance().isDevelopmentEnvironment())
        .then(
            CommandManager.literal("setOwner")
                .then(
                    CommandManager
                        .argument("targetLivingItem", EntityArgumentType.entity())
                        .then(
                            CommandManager.argument("targetOwner", EntityArgumentType.entity())
                                .executes(ctx -> {
                                    // ctx.getArgument("targetLivingItem", EntitySelector.class).getEntity(ctx.getSource());
                                    LivingItemEntity livingItem = ctx.getArgument("targetLivingItem", EntitySelector.class).getEntity(ctx.getSource()) instanceof LivingItemEntity living ? living : null;
                                    LivingEntity owner = ctx.getArgument("targetOwner", EntitySelector.class).getEntity(ctx.getSource()) instanceof LivingEntity le ? le : null;
                                    if (livingItem == null) {
                                        ctx.getSource().sendError(Text.of("targetLivingItem must be LivingItemEntity."));
                                    }
                                    if (owner == null) {
                                        ctx.getSource().sendError(Text.of("targetOwner must be at least a LivingEntity."));
                                    }
                                    livingItem.setOwner(owner);
                                    return 1;
                                })
                        )

                )
        )
        .then(
            CommandManager
                .literal("setEmote")
                .then(
                    CommandManager.argument("living", EntityArgumentType.entity())
                    .then(
                        CommandManager.argument("emote", StringArgumentType.word())
                        .suggests((ctx,builder) -> {
                            return CommandSource.suggestMatching(Stream.of(Emote.values()).map(emote -> emote.name()), builder);
                        })
                        .executes(ctx -> {
                            LivingItemEntity livingItem = ctx.getArgument("living", EntitySelector.class).getEntity(ctx.getSource()) instanceof LivingItemEntity living ? living : null;
                            Emote emote = Emote.valueOf(ctx.getArgument("emote", String.class));
                            livingItem.setEmote(emote);
                            return 1;
                        })
                    )
                )


        )
        ;

    static LiteralArgumentBuilder<ServerCommandSource> test_book = CommandManager
        .literal("test_book")
        .requires(s -> FabricLoader.getInstance().isDevelopmentEnvironment())
        .executes(context -> {
           context.getSource().getPlayer().giveItemStack(DoMagicThing.getBook()) ;
           return 0;
        });

    static LiteralArgumentBuilder<ServerCommandSource> moenchantmentsItems = CommandManager
        .literal("moenchant_items")
        .requires(s -> s.getEntity() instanceof ServerPlayerEntity && s.getEntity().hasPermissionLevel(2))
        .then(CommandManager.argument("item", IdentifierArgumentType.identifier())
            .suggests((context, builder) -> {
                return CommandSource.suggestIdentifiers(Registries.ITEM.getIds(), builder);
            })
            .then(CommandManager.literal(SuggestEnchantmentType.ENCHANT_RANDOMLY.name())
                .executes(serverCommandSourceCommandContext -> {
                    ServerPlayerEntity player = serverCommandSourceCommandContext.getSource().getPlayer();
                    Identifier tag = serverCommandSourceCommandContext.getArgument("item", Identifier.class);
                    List<ItemStack> list = Lists.newArrayList();
                    while(list.size() < 54) {
                        list.addAll(enchantedRandomly(LootHelper.itemEntryOf(Registries.ITEM.get(tag)), player));
                    }
                    Collections.shuffle(list);
                    try {
                        player.openHandledScreen(LootHelper.getScreenWithItems(list));
                    } catch(Throwable t) {
                        t.printStackTrace();
                    }
                    return 0;
                })
            )
            .then(CommandManager.literal(SuggestEnchantmentType.ENCHANT_WITH_LEVELS.name())
                .then(CommandManager.argument("level", IntegerArgumentType.integer(0, 255))
                    .then(CommandManager.argument("withTreasure", BoolArgumentType.bool())
                        .executes(serverCommandSourceCommandContext -> {
                            ServerPlayerEntity player = serverCommandSourceCommandContext.getSource().getPlayer();
                            int levels = serverCommandSourceCommandContext.getArgument("level", Integer.class);
                            Identifier tag = serverCommandSourceCommandContext.getArgument("item", Identifier.class);
                            boolean treasure = serverCommandSourceCommandContext.getArgument("withTreasure", Boolean.class);
                            List<ItemStack> list = Lists.newArrayList();
                            while(list.size() < 54) {
                                list.addAll(enchantWithLevels(LootHelper.itemEntryOf(Registries.ITEM.get(tag)), player, levels, treasure));
                            }
                            Collections.shuffle(list);
                            try {
                                player.openHandledScreen(LootHelper.getScreenWithItems(list));
                            } catch(Throwable t) {
                                t.printStackTrace();
                            }
                            return 0;
                        })
                    )
                )
            )
        )
        .executes(serverCommandSourceCommandContext -> {
            serverCommandSourceCommandContext.getSource().sendFeedback(Text.of("Nope"), false);
            return 0;
        });

    static LiteralArgumentBuilder<ServerCommandSource> moenchantments = CommandManager
        .literal("moenchantments")
        .requires(s -> s.getEntity() instanceof ServerPlayerEntity && s.getEntity().hasPermissionLevel(2))
        .then(CommandManager.argument("tag", StringArgumentType.word())
            .suggests((context, builder) -> {
                return CommandSource.suggestMatching(SuggestTag.strings(), builder);
            })
            .then(CommandManager.literal(SuggestEnchantmentType.ENCHANT_RANDOMLY.name())
                .executes(serverCommandSourceCommandContext -> {
                    ServerPlayerEntity player = serverCommandSourceCommandContext.getSource().getPlayer();
                    String tag = serverCommandSourceCommandContext.getArgument("tag", String.class);
                    List<ItemStack> list = Lists.newArrayList();
                    while(list.size() < 54) {
                        list.addAll(enchantedRandomly(LootHelper.tagEntryOf(SuggestTag.valueOf(tag).tag, false), player));
                    }
                    Collections.shuffle(list);
                    try {
                        player.openHandledScreen(LootHelper.getScreenWithItems(list));
                    } catch(Throwable t) {
                        t.printStackTrace();
                    }
                    return 0;
                })
            )
            .then(CommandManager.literal(SuggestEnchantmentType.ENCHANT_WITH_LEVELS.name())
                .then(CommandManager.argument("level", IntegerArgumentType.integer(0, 255))
                    .then(CommandManager.argument("withTreasure", BoolArgumentType.bool())
                        .executes(serverCommandSourceCommandContext -> {
                            ServerPlayerEntity player = serverCommandSourceCommandContext.getSource().getPlayer();
                            int levels = serverCommandSourceCommandContext.getArgument("level", Integer.class);
                            String tag = serverCommandSourceCommandContext.getArgument("tag", String.class);
                            boolean treasure = serverCommandSourceCommandContext.getArgument("withTreasure", Boolean.class);
                            List<ItemStack> list = Lists.newArrayList();
                            // list = enchantmentRandomlyTable(player.getRandom(), levels, treasure);
                            while(list.size() < 54) {
                                try {
                                    list.addAll(enchantWithLevels(LootHelper.tagEntryOf(SuggestTag.valueOf(tag).tag, false), player, levels, treasure));
                                }catch(Throwable t) {
                                    t.printStackTrace();
                                }
                            }
                            Collections.shuffle(list);
                            try {
                                player.openHandledScreen(LootHelper.getScreenWithItems(list));
                            } catch(Throwable t) {
                                t.printStackTrace();
                            }
                            return 0;
                        })
                    )
                )
            )
        )
        .executes(serverCommandSourceCommandContext -> {
            serverCommandSourceCommandContext.getSource().sendFeedback(Text.of("Nope"), false);
            return 0;
        });

    private static List<ItemStack> enchantedRandomly(LootPoolEntry.Builder<?> builder, ServerPlayerEntity player) {
        // LootHelper.poolBuilder().rolls(LootHelper.constantRange(1));
        return LootHelper.tableBuilder()
        .type(LootContextTypes.CHEST)
        .pool(LootHelper
            .poolBuilder()
            .rolls(LootHelper.constantRange(1))
            .with(builder)
            .apply(EnchantRandomlyLootFunction.builder())
        ).build().generateLoot(LootHelper.newContext(player));
    }
    private static List<ItemStack> enchantWithLevels(LootPoolEntry.Builder<?> builder, ServerPlayerEntity player, int levels, boolean withTreasure) {
        EnchantWithLevelsLootFunction.Builder function = EnchantWithLevelsLootFunction.builder(LootHelper.constantRange(levels));
        if (withTreasure) function.allowTreasureEnchantments();
        return LootHelper.tableBuilder()
        .type(LootContextTypes.CHEST)
        .pool(LootHelper
            .poolBuilder()
            .rolls(LootHelper.constantRange(1))
            .with(builder)
            .apply(function))
        .build()
        .generateLoot(LootHelper.newContext(player));
    }

    @SuppressWarnings({"unused"})
    private static List<ItemStack> enchantmentRandomlyTable(Random random, int power, boolean treasureAllowed) {
        List<ItemStack> stacks = Lists.newArrayList();
        // Random r = new Random(1);
        Stopwatch sw = Stopwatch.createStarted();
        for (int i = 0; i < 54; i++) {
            ItemStack stack = new ItemStack(Items.NETHERITE_BOOTS);
            EnchantmentHelper.generateEnchantments(random, stack, power, treasureAllowed).forEach(e -> stack.addEnchantment(e.enchantment, e.level));

            // EnchantmentTable.EnchantmentHelper$generateEnchantment(random, stack, power, treasureAllowed).forEach(set -> stack.addEnchantment(set.enchantment, set.level));
            stacks.add(stack);
        }
        sw.stop();
        System.out.println(sw.elapsed(TimeUnit.MICROSECONDS) / 54F);
        return stacks;
    }
}
