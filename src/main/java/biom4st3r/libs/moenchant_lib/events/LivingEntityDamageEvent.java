package biom4st3r.libs.moenchant_lib.events;

import net.minecraft.util.TypedActionResult;

import org.jetbrains.annotations.ApiStatus.Internal;

/**
 * LivingEntityDamageCallback
 */
@Internal
public interface LivingEntityDamageEvent {
    static final TypedActionResult<Float> PASS = TypedActionResult.pass(0F);
    static final TypedActionResult<Float> SUCCESS = TypedActionResult.success(0F);
    static final TypedActionResult<Float> FAIL = TypedActionResult.fail(0F);
}