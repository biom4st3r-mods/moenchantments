package biom4st3r.libs.moenchant_lib.builder;

enum Booleans {
    ENABLED,
    IS_TREASURE,
    IS_CURSE,
    TRANSFERS_FROM_BOW_TO_ARROW,
    TRANSFERS_FROM_CROSSBOW_TO_ARROW,
    IS_AVAILABLE_FOR_ENCHANTED_BOOK_OFFER,
    IS_AVAILABLE_FOR_RANDOM_SELECTION,
    HAS_APPLY_LOGIC,
    IS_ACCEPTIBLE_ON_BOOK,
    ;
    public boolean get(EnchantmentSkeleton skele) {
        return skele.get(this);
    }
    public void set(EnchantmentSkeleton skele, boolean b) {
        skele.set(this, b);
    }
}