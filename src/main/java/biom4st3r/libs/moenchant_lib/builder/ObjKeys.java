package biom4st3r.libs.moenchant_lib.builder;

import java.util.List;
import java.util.Map;
import java.util.function.BiFunction;
import java.util.function.Consumer;
import java.util.function.IntFunction;
import java.util.function.Predicate;

import com.google.common.collect.Multimap;

import biom4st3r.libs.moenchant_lib.ExtendedEnchantment.AnvilAcceptibleContext;
import biom4st3r.libs.moenchant_lib.ExtendedEnchantment.ModifyDamageFunction;
import biom4st3r.libs.moenchant_lib.ExtendedEnchantment.TriConsumer;
import biom4st3r.libs.moenchant_lib.builder.EnchantBuilder.BlockBreakContext;
import biom4st3r.libs.moenchant_lib.builder.EnchantBuilder.PseudoGetPossibleEnchantments;
import biom4st3r.libs.moenchant_lib.events.OnBowArrowCreationEvent;
import biom4st3r.libs.moenchant_lib.events.OnCrossBowArrowCreationEvent;
import it.unimi.dsi.fastutil.ints.Int2IntFunction;
import net.minecraft.enchantment.Enchantment;
import net.minecraft.entity.Entity;
import net.minecraft.entity.EntityGroup;
import net.minecraft.entity.EquipmentSlot;
import net.minecraft.entity.LivingEntity;
import net.minecraft.entity.attribute.EntityAttribute;
import net.minecraft.entity.attribute.EntityAttributeModifier;
import net.minecraft.entity.damage.DamageSource;
import net.minecraft.entity.data.TrackedData;
import net.minecraft.item.ItemStack;
import net.minecraft.loot.context.LootContext;
import net.minecraft.screen.AnvilScreenHandler;
import net.minecraft.text.Text;

final class ObjKeys {
    public static int size() {
        return Key.index + 1;
    }
    record Key<T>(int ordinal) {
        static int index = 0;
        public static <T> Key<T> newKey() {
            Key<T> key = new Key<T>(index);
            index++;
            return key;
        }
        public void set(EnchantmentSkeleton skeleton, T t) {
            skeleton.set(this, t);
            // skeleton.OBJS[this.ordinal()] = t;
        }
        public T get(EnchantmentSkeleton skeleton) {
            return skeleton.get(this);
            // return (T) skeleton.OBJS[this.ordinal()];
        }
    }
    public static final Key<String> REG_NAME = Key.newKey();
    public static final Key<Predicate<ItemStack>> IS_ACCEPTIBLE = Key.newKey();
    public static final Key<BiFunction<ItemStack, AnvilScreenHandler, Boolean>> IS_ACCEPTIBLE_IN_ANVIL = Key.newKey();
    public static final Key<PseudoGetPossibleEnchantments> APPLY_LOGIC = Key.newKey();
    public static final Key<Int2IntFunction> MIN_POWER = Key.newKey();
    public static final Key<Int2IntFunction> MAX_POWER = Key.newKey();
    public static final Key<List<Enchantment>> EXCLUSIVE_ENCHANTMENTS = Key.newKey();
    public static final Key<BiFunction<ItemStack, LootContext, Boolean>> IS_ACCEPTABLE_FOR_RANDOM_LOOT_FUNC = Key.newKey();
    public static final Key<Consumer<ItemStack>> WHEN_ADDED_TO_STACK = Key.newKey();
    public static final Key<TrackedData<Byte>> PROJECTILE_TRACKED_DATA = Key.newKey();
    public static final Key<TriConsumer<LivingEntity, Entity, Integer>> ON_TARGET_DAMAGED = Key.newKey();
    public static final Key<TriConsumer<LivingEntity, Entity, Integer>> ON_USER_DAMAGED = Key.newKey();
    public static final Key<BiFunction<Integer, DamageSource, Integer>> GET_PROTECTION_AMOUNT = Key.newKey();
    public static final Key<BiFunction<Integer, EntityGroup, Float>> GET_ATTACK_DAMAGE = Key.newKey();
    public static final Key<ModifyDamageFunction> MODIFY_INCOMING_DAMAGE = Key.newKey();
    public static final Key<ModifyDamageFunction> MODIFY_DAMAGE_OUTPUT = Key.newKey();
    public static final Key<Map<EquipmentSlot, Multimap<EntityAttribute, EntityAttributeModifier>>> ENTITY_ATTRIBUTES = Key.newKey();
    public static final Key<AnvilAcceptibleContext> IS_ACCEPTIBLE_IN_ANVIL_2 = Key.newKey();
    public static final Key<BlockBreakContext> PRE_BLOCK_BREAK = Key.newKey();
    public static final Key<BlockBreakContext> POST_BLOCK_BREAK = Key.newKey();
    public static final Key<OnCrossBowArrowCreationEvent> ON_CROSSBOW_ARROW_CREATED = Key.newKey();
    public static final Key<OnBowArrowCreationEvent> ON_BOW_ARROW_CREATED = Key.newKey();
    public static final Key<IntFunction<Text>> GET_NAME = Key.newKey();
}