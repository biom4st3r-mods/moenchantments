package biom4st3r.libs.moenchant_lib;

import java.lang.invoke.MethodHandles;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.lang.reflect.Modifier;

import net.minecraft.enchantment.EnchantmentTarget;

import net.fabricmc.loader.api.FabricLoader;

import com.google.common.base.Preconditions;
import org.jetbrains.annotations.ApiStatus.Internal;
import org.objectweb.asm.ClassVisitor;
import org.objectweb.asm.ClassWriter;
import org.objectweb.asm.MethodVisitor;
import org.objectweb.asm.Opcodes;
import org.objectweb.asm.Type;

@Internal
public final class AsmEnchantmentTarget implements Opcodes {

    public static boolean do0False0(Object i) {
        return false;
    }

    @Internal
    public static EnchantmentTarget addTarget(String name, Method method) {
        Preconditions.checkArgument(Type.getReturnType(method) == Type.BOOLEAN_TYPE);
        Type[] args = Type.getArgumentTypes(method);
        Preconditions.checkArgument(args.length == 1);
        Preconditions.checkArgument(args[0].equals(Type.getType(Object.class)));
        Preconditions.checkArgument((method.getModifiers() & Modifier.STATIC) == Modifier.STATIC);

        EnchantmentTarget target = makeEnchantmentTarget(EnchantmentTarget.values().length, name, method);
        // EnchantmentTarget[] enums = addEnum(EnchantmentTarget.class, target);
        // fixClassContants(EnchantmentTarget.class, enums);
        return target;
    }

    private static String NAMESPACE = "intermediary";//FabricLoader.getInstance().getMappingResolver().getCurrentRuntimeNamespace();

    private static String classRemap(String s) {
        return FabricLoader.getInstance().getMappingResolver().mapClassName(NAMESPACE, s.replace('/', '.')).replace('.', '/');
    }

    @SuppressWarnings({"unused"})
    private static String fieldRemap(String owner, String name, String descriptor) {
        return FabricLoader.getInstance().getMappingResolver().mapFieldName(NAMESPACE, owner.replace('/', '.'), name, descriptor).replace('.', '/');
    }

    private static String methodRemap(String owner, String name, String descriptor) {
        return FabricLoader.getInstance().getMappingResolver().mapMethodName(NAMESPACE, owner.replace('/', '.'), name, descriptor).replace('.', '/');
    }

    private static final String
        CLASS$ENCHANTMENT_TARGET = classRemap("net/minecraft/class_1886"),
        CLASS$ITEM = classRemap("net/minecraft/class_1792"),
        DESCRIPTOR$ITEM_BOOLEAN = "(L" + CLASS$ITEM + ";)Z",
        METHOD$IS_ACCEPTIBLE_ITEM = methodRemap(CLASS$ENCHANTMENT_TARGET, "method_8177", DESCRIPTOR$ITEM_BOOLEAN)
        ;

    // Just a wittle bit of asm (づ｡◕‿‿◕｡)づ
    private static EnchantmentTarget makeEnchantmentTarget(int i, String name, Method method) {
        ClassWriter writer = new ClassWriter(ClassWriter.COMPUTE_MAXS);
        ClassVisitor visitor = new ClassVisitor(ASM8,writer) {};
        FabricLoader.getInstance().getMappingResolver();
        visitor.visit(V16, ACC_PUBLIC|ACC_FINAL, "biom4st3r/libs/moenchant_lib/CustomEnchantmentTarget", null, CLASS$ENCHANTMENT_TARGET, null);
        MethodVisitor mv = visitor.visitMethod(ACC_PUBLIC, "<init>", "()V", null, null);
        {
            mv.visitCode();
            mv.visitVarInsn(ALOAD, 0);
            mv.visitLdcInsn(name);
            if(i <= Byte.MAX_VALUE && i >= Byte.MIN_VALUE) {
                mv.visitIntInsn(Opcodes.BIPUSH, i);
            } else if(i <= Short.MAX_VALUE && i >= Short.MIN_VALUE) {
                mv.visitIntInsn(Opcodes.SIPUSH, i);
            } else {
                mv.visitLdcInsn(i);
            }
            
            mv.visitMethodInsn(INVOKESPECIAL, CLASS$ENCHANTMENT_TARGET, "<init>", "(Ljava/lang/String;I)V", false);
            mv.visitInsn(RETURN);
            mv.visitMaxs(1, 1);
            mv.visitEnd();
        }
        mv = visitor.visitMethod(ACC_PUBLIC, METHOD$IS_ACCEPTIBLE_ITEM, DESCRIPTOR$ITEM_BOOLEAN, null, null);
        {
            mv.visitCode();
            mv.visitVarInsn(ALOAD, 1);
            mv.visitMethodInsn(INVOKESTATIC, Type.getInternalName(method.getDeclaringClass()), method.getName(), Type.getMethodDescriptor(method), false);
            mv.visitInsn(IRETURN);
            mv.visitMaxs(1, 1);
            mv.visitEnd();
        }
        byte[] clazz = writer.toByteArray();
        Class<?> ENUM = null;
        try {
            ENUM = MethodHandles.lookup().defineClass(clazz);
        } catch (IllegalAccessException e) {
            throw new RuntimeException(e);
        }

        EnchantmentTarget target = null;
        try {
            target = (EnchantmentTarget) ENUM.getDeclaredConstructors()[0].newInstance();
        } catch (InstantiationException | IllegalAccessException | IllegalArgumentException | InvocationTargetException
                | SecurityException e) {
            throw new RuntimeException(e);
        }
        System.out.println(target);
        return target;
    }
}
