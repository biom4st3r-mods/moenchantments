package biom4st3r.libs.particle_emitter;

import net.fabricmc.api.ModInitializer;

public class LibInit implements ModInitializer {
    public static final String MODID = "particle_emitter";

    @Override
    public void onInitialize() {
        Packets.classLoad();
    }
}
