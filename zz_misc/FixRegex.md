# Incorrect Spacing on commas (arg0,arg1,arg2) -> (arg0, arg1, arg2)
* (.),(\w)
* $1, $2
# Replace C style brackets with java
* \)[ ]{0,}\n[ ]{0,}\{
* ) {
# Removed brackets from single arg lambda
* \(([a-zA-Z0-9]{1,})\)(\W{0,})->
* $1$2->
# Replace (notspace)-> with (notspace) ->
* ([^\s])->
* $1 ->
# Replace 1line lambda bodies (asdf)->{return true;}
* ->([ ]{0,})\{return (.*);\}
* ->$1$2
# for|if followed by non-space
* ( )(for|if)(\()
* $1$2 $3
# Replace ->(not whitespace)
* ->([^ ])
* -> $1
# Add spaces to =
* ([a-zA-Z0-9])=([^ ])
* $1 = $2
# Cleanup arithmatic (WIP)
* ([^\+^\-^/^*^%^ ^"^'])([\+\-/*%][=]{0,})([^ ^\+^\-^/^*^%^;^"^'])
* $1 $2 $3